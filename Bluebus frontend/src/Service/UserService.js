import axios from 'axios'

const USERS_REST_API_URL = 'http://localhost:7070'

class UserServices {
  getUsers(user) {
    console.log('in API')
    return axios.post('' + USERS_REST_API_URL + '/signin', user)
  }
  addUsers(user) {
    return axios.post('' + USERS_REST_API_URL + '/signup', user)
  }
  signInOwner(user) {
    console.log('in API')
    return axios.post('' + USERS_REST_API_URL + '/owner/signin', user)
  }
  addBusByOwner(bus) {
    return axios.post(
      '' +
        USERS_REST_API_URL +
        '/owner/addbus/byownerid/' +
        window.localStorage.getItem('comId'),
      bus
    )
  }
  addOwner(user) {
    return axios.post('' + USERS_REST_API_URL + '/admin/addowner', user)
  }

  addCarByOwner(car) {
    return axios.post(
      '' + USERS_REST_API_URL + '/owner/addcar/byownerid/1',
      car
    )
  }

  searchBus(req) {
    return axios.post('' + USERS_REST_API_URL + '/getavailablebuses', req)
  }
  searchCar(req) {
    return axios.post('' + USERS_REST_API_URL + '/show/carsbycity', req)
  }
  getCompanies() {
    return axios.get('' + USERS_REST_API_URL + '/admin/getallowner')
  }
  carBooking(user) {
    return axios.post('' + USERS_REST_API_URL + '/addcarbooking/'+window.localStorage.getItem("carId")+'/'+window.localStorage.getItem("userId"), user)
  }
  getBusByOwner() {
    return axios.get('' + USERS_REST_API_URL + '/owner/getbuses/byownerid/'+window.localStorage.getItem("comId"))
  }
  getCarByOwner() {
    return axios.get('' + USERS_REST_API_URL + '/owner/getcars/byownerid/'+window.localStorage.getItem("comId"))
  }
}

export default new UserServices()
