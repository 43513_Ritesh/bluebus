import { CssBaseline, Grid, Paper, TextField, Button } from '@material-ui/core'
import React from 'react'
import UserService from '../Service/UserService'
import Image from '../img/bg.jpg'

class BusBooking extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            startDate:'',
            endDate:'',
            Kms:''
        }
        this.confirm = this.confirm.bind(this)
    this.handleKms = this.handleKms.bind(this)
    this.handleStartDate = this.handleStartDate.bind(this)
    this.handleEndDate = this.handleEndDate.bind(this)
    }

    confirm = (e) => {
        e.preventDefault()
        let user = {
            startDate: this.state.startDate,
            Kms: this.state.Kms,
        }
        UserService.carBooking(user).then((res) => {
          
        })
      }
    

    handleKms = (e) => this.setState({ Kms: e.target.value })
  
  handleEndDate = (e) => this.setState({ endDate: e.target.value })

    render() {
        const tabStyle = {
          padding: 40,
          height: '70vh',
          width: 340,
          margin: '0px auto',
        }
    
        const buttonStyle = {
          margin: '20px auto',
        }
    
        const avatarStyle = {
          backgroundColor: '#4db2b5',
        }
    
        const bgStyle = {
          backgroundImage: `url(${Image})`,
          minHeight: '100vh',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }
    
        const paperStyle = {
          margin: '0px auto',
        }
        const ownerbtn = {
          backgroundColor: '',
        }
    
        return (
          <Grid style={bgStyle}>
            <CssBaseline />
            <Grid style={paperStyle}>
              <Paper elevation={20} style={tabStyle} align="center">
                
                <h2>Bus Booking</h2>
                <form>
                <TextField
                    label="Seats"
                    onChange={this.handleKms}
                    fullWidth
                  />
                 <TextField
                    label="Trip Date"
                    defaultValue="2000-01-01"
                    type="date"
                    onChange={this.handleEndDate}
                    fullWidth
                  />
                </form>
                <Button
                  onClick={this.confirm}
                  style={buttonStyle}
                  variant="contained"
                  color="primary"
                  fullWidth>
                  Confirm
                </Button>
              </Paper>
            </Grid>
          </Grid>
        )
      }
}

export default BusBooking