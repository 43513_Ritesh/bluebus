import { Grid } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Paper from '@material-ui/core/Paper'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import Typography from '@material-ui/core/Typography'
import React, { useState } from 'react'
import Image from '../../img/admin.jpg'
import AboutUs from '../AboutUs'
import AllBookings from '../Admin/Home'
import ContactUs from '../ContactUs'
import Addbus from '../Owner/Bus/AddBus'
import AllBuses from '../Owner/Bus/AllBuses'
import AddCar from '../Owner/Car/AddCar'
import AllCars from '../Owner/Car/AllCar'
import Footer from '../Footer'
import Profile from '../Customer/Profile'

const OwnerNavbar = () => {
  const [value, setValue] = useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}>
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    )
  }

  const navStyle = {
    backgroundColor: '#ff668c',
    padding: 10,
  }

  const bgStyle = {
    backgroundImage: `url(${Image})`,
    minHeight: '100vh',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  }

  return (
    <Grid>
      <Paper>
        <Tabs
          style={navStyle}
          value={value}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="secondary"
          //textColor="primary"
          onChange={handleChange}
          aria-label="disabled tabs example">
          <Tab label="Add Bus" />
          <Tab label="Add Car" />
          <Tab label="Buses" />
          <Tab label="Cars" />
          <Tab label="All Bookings" />
          <Tab label="About Us" />
          <Tab label="Contact Us" />
          <Tab label="Profile" />
        </Tabs>
        <TabPanel style={bgStyle} value={value} index={0}>
          <Addbus />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={1}>
          <AddCar />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={2}>
         <AllBuses />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={3}>
        <AllCars />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={4}>
           <AllBookings />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={5}>
          <AboutUs />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={6}>
          <ContactUs />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={7}>
          <Profile />
        </TabPanel>
      </Paper>
<Footer/>    
    </Grid>
  )
}

export default OwnerNavbar
