import { Grid } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import React, { useState } from "react";
import Image from "../../img/admin.jpg";
import AboutUs from "../AboutUs";
import ContactUs from "../ContactUs";
import CustomerHome from "../Customer/CustomerHome";
import MyTrips from "../Customer/MyTrips";
import Profile from "../Customer/Profile";
import SearchBus from "../Customer/SearchBus";
import SearchCar from "../Customer/SearchCar";
import Footer from "../Footer";
const CustomerNavbar = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  const navStyle = {
    backgroundColor: "#ff668c",
    padding: 10,
  };
  const bgStyle = {
    backgroundImage: `url(${Image})`,
    minHeight: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };

  return (
    <Grid>
      <Paper>
        <Tabs
          style={navStyle}
          value={value}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="secondary"
          // textColor="secondary"
          onChange={handleChange}
          aria-label="disabled tabs example"
        >
          <Tab label="Home" />
          <Tab label="Search Bus" />
          <Tab label="Search Car" />
          <Tab label="ABOUT Us" />
          <Tab label="Contact Us" />
          {/* <Tab label="My Trips" /> */}
          <Tab label="Profile" />
        </Tabs>
        <TabPanel style={bgStyle} value={value} index={0}>
          <CustomerHome />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={1}>
          <SearchBus />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={2}>
          <SearchCar />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={3}>
          <AboutUs />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={4}>
          <ContactUs />
        </TabPanel>
        {/* <TabPanel style={bgStyle} value={value} index={5}>
          <MyTrips />
        </TabPanel> */}
        <TabPanel style={bgStyle} value={value} index={5}>
            <Profile />
          </TabPanel>
      </Paper>
      <Footer />
    </Grid>
  );
};

export default CustomerNavbar;
