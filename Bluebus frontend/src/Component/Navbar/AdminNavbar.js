import { Grid } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import React, { useState } from "react";
import Image from "../../img/admin.jpg";
import AdminHome from "../Admin/AdminHome";
import AllCompanies from "../Admin/AllCompanies";
import Home from "../Admin/Home";
import Footer from "../Footer";
import Profile from '../Customer/Profile'

const AdminNavbar = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  const navStyle = {
    backgroundColor: "#ff668c",
    padding: 10,
  };
  const bgStyle = {
    backgroundImage: `url(${Image})`,
    minHeight: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };

  return (
    <Grid>
      <Paper>
        <Tabs
          style={navStyle}
          value={value}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="secondary"
          // textColor="secondary"
          onChange={handleChange}
          aria-label="disabled tabs example"
        >
          <Tab label="Home" />
          <Tab label="Add Company" />
          <Tab label="All Companies" />
          <Tab label="Profile" />
          {/* <Tab label="About Us" />
          <Tab label="Contact Us" /> */}
        </Tabs>
        <TabPanel style={bgStyle} value={value} index={0}>
          <Home />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={1}>
          <AdminHome />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={2}>
          <AllCompanies />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={3}>
            <Profile />
        </TabPanel>
        {/* <TabPanel style={bgStyle} value={value} index={3}>
          <AboutUs />
        </TabPanel>
        <TabPanel style={bgStyle} value={value} index={4}>
          <ContactUs />
        </TabPanel> */}
      </Paper>
      <Footer />
    </Grid>
  );
};

export default AdminNavbar;
