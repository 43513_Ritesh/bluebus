import { CssBaseline, Grid, Paper, TextField, Button, Link } from '@material-ui/core'
import React from 'react'
import UserService from '../Service/UserService'
import Image from '../img/bg.jpg'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

class CheckOut extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            startDate:'',
            endDate:'',
            Kms:''
        }
       // this.confirm = this.confirm.bind(this)
    }

    // confirm = (e) => {
    //     e.preventDefault()
    //     let user = {
    //         startDate: this.state.startDate,
    //         endDate: this.state.endDate,
    //         Kms: this.state.Kms,
    //     }

    render() {
        const tabStyle = {
          padding: 40,
          height: '70vh',
          width: 340,
          margin: '0px auto',
        }
    
        const buttonStyle = {
          margin: '20px auto',
        }
    
        const avatarStyle = {
          backgroundColor: '#4db2b5',
        }
    
        const bgStyle = {
          backgroundImage: `url(${Image})`,
          minHeight: '100vh',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }
    
        const paperStyle = {
          margin: '0px auto',
        }
        const ownerbtn = {
          backgroundColor: '',
        }
    
        return (
          <Grid style={bgStyle}>
            <CssBaseline />
            <Grid style={paperStyle}>
              <Paper elevation={20} style={tabStyle} align="center">
                
                <h2>BOOKING</h2>
                <Grid>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">busName</TableCell>
                      <TableCell align="center">busNo</TableCell>
                      <TableCell align="center">driverName</TableCell>
                      <TableCell align="center">source</TableCell>
                      <TableCell align="center">destination</TableCell>
                      <TableCell align="center">time</TableCell>
                      <TableCell align="center">farePerSeat</TableCell>
                      <TableCell align="center">Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.buses.map((bus) => (
                      <TableRow>
                        <TableCell component="th" scope="row">
                          {bus.busName}{' '}
                        </TableCell>
                        <TableCell align="center">{bus.busNo}</TableCell>
                        <TableCell align="center">{bus.driverName}</TableCell>
                        <TableCell align="center">{bus.source}</TableCell>
                        <TableCell align="center">{bus.destination}</TableCell>
                        <TableCell align="center">{bus.farePerSeat}</TableCell>
                        <TableCell align="center">{bus.time}</TableCell>
                        <TableCell align="center"><Link href="/signin"><Button color="secondary">BOOK</Button></Link> </TableCell>                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
              </Paper>
            </Grid>
          </Grid>
        )
      }
}

export default CheckOut