import {
  CssBaseline,
  Grid,
  Paper,
  TextField,
  Button,
  Link,
} from "@material-ui/core";
import React from "react";
import UserService from "../Service/UserService";
import Image from "../img/bg.jpg";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

class CarBooking extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: "",
      endDate: "",
      kms: "",
      booking: {
        bookingId : 0,
        bookingDate : "",
        payment : 0.0,
        kms : 0.0,
        days : 0
      }
    };
    this.confirm = this.confirm.bind(this);
    this.payment = this.payment.bind(this);
    this.handlekms = this.handlekms.bind(this);
    this.handleStartDate = this.handleStartDate.bind(this);
    this.handleEndDate = this.handleEndDate.bind(this);
  }

  payment = (e) => {
    this.props.history.push('/transaction')
  }

  confirm = (e) => {
    e.preventDefault();
    let user = {
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      kms: this.state.kms,
    };
    // let carIdFromUrl = Request.QueryString["car.carId"] ?? string.Empty;
    // console.log(carIdFromUrl)
    UserService.carBooking(user).then((res) => {
      const booking = res.data;
      this.setState({ booking });
    });
  };

  handlekms = (e) => this.setState({ kms: e.target.value });
  handleStartDate = (e) => this.setState({ startDate: e.target.value });
  handleEndDate = (e) => this.setState({ endDate: e.target.value });

  render() {
    const tabStyle = {
      padding: 40,
      height: "70vh",
      width: 340,
      margin: "0px auto",
    };

    const buttonStyle = {
      margin: "20px auto",
    };

    const avatarStyle = {
      backgroundColor: "#4db2b5",
    };

    const bgStyle = {
      backgroundImage: `url(${Image})`,
      minHeight: "100vh",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    };

    const paperStyle = {
      margin: "0px auto",
    };
    const ownerbtn = {
      backgroundColor: "",
    };

    const tableStyle = {
      margin: "20px auto",
      width : 700,
      background: "none"
      
    }

    return (
      <Grid style={bgStyle}>
        <CssBaseline />
        <Grid style={paperStyle}>
          <Paper elevation={20} style={tabStyle} align="center">
            <h2>BOOKING</h2>
            <form>
              <TextField label="kms" onChange={this.handlekms} fullWidth />
              <TextField
                defaultValue="2000-01-01"
                label="Start Date"
                type="date"
                onChange={this.handleStartDate}
                fullWidth
              />
              <TextField
                label="End Date"
                defaultValue="2000-01-01"
                type="date"
                onChange={this.handleEndDate}
                fullWidth
              />
            </form>
            <Link href="/checkout">
              <Button
                onClick={this.confirm}
                style={buttonStyle}
                variant="contained"
                color="primary"
                fullWidth
              >
                Confirm
              </Button>
            </Link>
          </Paper>
        </Grid>
        <Paper elevation={20} style={tableStyle}>
          <Grid>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Booking ID</TableCell>
                    <TableCell align="center">Booking Date</TableCell>
                    <TableCell align="center">Days</TableCell>
                    <TableCell align="center">Kms</TableCell>
                    <TableCell align="center">Payment</TableCell>
                    <TableCell align="center"></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {/* {this.state.booking.map((book) => ( */}
                  <TableRow>
                    <TableCell align="center" component="th" scope="row">
                      {this.state.booking.bookingId}
                    </TableCell>
                    <TableCell align="center">{this.state.booking.bookingDate}</TableCell>
                    <TableCell align="center">{this.state.booking.days}</TableCell>
                    <TableCell align="center">{this.state.booking.kms}</TableCell>
                    <TableCell align="center">{this.state.booking.payment}</TableCell>
                    <TableCell align="center"> <Button color="primary" variant="outlined" onClick={this.payment}>Proceed To Payment</Button> </TableCell>
                    <TableCell align="center"></TableCell>
                  </TableRow>
                  {/* // ))} */}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

export default CarBooking;
