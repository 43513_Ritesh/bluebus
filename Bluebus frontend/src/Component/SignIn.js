import UserServices from "../Service/UserService";
import {
  Avatar,
  Button,
  CssBaseline,
  Grid,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Link from "@material-ui/core/Link";
import Image from "../img/bg.jpg";

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    this.signin = this.signin.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  signin = (e) => {
    e.preventDefault();
    // window.localStorage.clear();
    let user = {
      email: this.state.email,
      password: this.state.password,
    };

    UserServices.getUsers(user).then((res) => {
      console.log(res);
      let u = res.data;
      window.localStorage.setItem("userId", u.userId);
      window.localStorage.setItem("firstName", u.firstName);
      window.localStorage.setItem("lastName", u.lastName);
      window.localStorage.setItem("email", u.email);
      window.localStorage.setItem("password", u.password);
      window.localStorage.setItem("phone", u.phone);
      window.localStorage.setItem("gender", u.gender);
      window.localStorage.setItem("dob", u.dob);
      window.localStorage.setItem("role", u.role);

      if (res.data.role === "CUSTOMER") {
        this.props.history.push("/carbooking");
      } else {
        this.props.history.push("/adminnavbar");
      }
    });
  };

  handleChange = (e) => this.setState({ email: e.target.value });
  handleChange2 = (e) => this.setState({ password: e.target.value });

  render() {
    const tabStyle = {
      padding: 40,
      height: "70vh",
      width: 340,
      margin: "0px 1050px",
    };

    const buttonStyle = {
      margin: "20px auto",
    };

    const avatarStyle = {
      backgroundColor: "#4db2b5",
    };

    const bgStyle = {
      backgroundImage: `url(${Image})`,
      minHeight: "100vh",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    };

    const paperStyle = {
      margin: "0px auto",
    };
    const ownerbtn = {
      backgroundColor: "",
    };

    return (
      <Grid style={bgStyle}>
        <CssBaseline />
        <Grid style={paperStyle}>
          <Paper elevation={10} style={tabStyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
            <h2>Sign In</h2>
            <form>
              <TextField label="Email" onChange={this.handleChange} fullWidth />
              <TextField
                label="Password"
                type="password"
                onChange={this.handleChange2}
                fullWidth
              />
            </form>
            <Button
              onClick={this.signin}
              style={buttonStyle}
              variant="contained"
              color="primary"
              fullWidth
            >
              Sign In
            </Button>
            <Typography>
              Don't Have an Account ?<Link href="/Signup">Signup Here</Link>
            </Typography>
            <Typography>
              <br />
              <br />
              <br />
              <br />
              <Link href="/ownersignin">
                <Button variant="contained" color="inherit" style={ownerbtn}>
                  SignIn as Owner
                </Button>
              </Link>
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default SignIn;
