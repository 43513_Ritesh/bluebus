import { Button, Grid, TextField, Link } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import UserService from "../../Service/UserService";

class SearchBus extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      source: "",
      destination: "",
      buses: [],
    };

    this.search = this.search.bind(this);
    this.handleSource = this.handleSource.bind(this);
    this.handleDestination = this.handleDestination.bind(this);
    this.handleDate = this.handleDate.bind(this);
   
  }

  search = (e) => {
    e.preventDefault();
    let req = {
      source: this.state.source,
      destination: this.state.destination,
    };
    UserService.searchBus(req).then((res) => {
      const buses = res.data;
      this.setState({ buses });
      // this.props.history.push('/adminnavbar')
    });
  };

  handleSource = (e) => {
    this.setState({ source: e.target.value });
  };
  handleDestination = (e) => {
    this.setState({ destination: e.target.value });
  };
  handleDate = (e) => {
    this.setState({ date: e.target.value });
  };

  render() {
    const avatarStyle = {
      backgroundColor: "#ff5252",
    };

    const tabStyle = {
      padding: 8,
    };

    const paperStyle = {
      padding: 30,
      height: "100vh",
      width: 1100,
      margin: "0px auto",
      background: "none",
      backdropFilter: "blur(10px)",
    };
    const btnStyle = {
      padding: 10,
      margin: "40px 200px 0px 150px",
      width: 200,
      height: 60,
      backgroundColor: "#6fbf73",
    };

    const serachStyle = {
      margin: "20px 0px 0px 50px",
    };

    const searchbtn = {
      margin: "20px 0px 0px 50px",
    };

    const tableStyle = {
      margin: "40px 0px 0px 0px",
      background: "none",
    };

    // function createData(bus_name, bus_no, source, destination, Fare, Time) {
    //   return {
    //     bus_name,
    //     bus_no,
    //     source,
    //     destination,
    //     Fare,
    //     Time,
    //   }
    // }

    // const buses = [
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    // ]

    return (
      <Grid>
        <Grid>
          <Paper elevation={10} style={paperStyle}>
            <Grid align="center">
              <TextField
                style={serachStyle}
                onChange={this.handleSource}
                label="Source"
                variant="outlined"
                color="primary"
              >
                Source
              </TextField>
              <TextField
                style={serachStyle}
                onChange={this.handleDestination}
                label="Destination"
                variant="outlined"
                color="primary"
              >
                Destination
              </TextField>
              <TextField
                style={serachStyle}
                onChange={this.handleDate}
                type="date"
                label="date"
                defaultValue="2000-01-01"
                variant="filled"
                color="primary"
              ></TextField>
            </Grid>

            <Grid align="center">
              <Button
                onClick={this.search}
                style={searchbtn}
                variant="filled"
                color="secondary"
              >
                SEACRH
              </Button>
            </Grid>
            <Grid>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">busName</TableCell>
                      <TableCell align="center">busNo</TableCell>
                      <TableCell align="center">driverName</TableCell>
                      <TableCell align="center">source</TableCell>
                      <TableCell align="center">destination</TableCell>
                      <TableCell align="center">farePerSeat</TableCell>
                      <TableCell align="center">Time</TableCell>
                      <TableCell align="center">Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.buses.map((bus) => (
                      <TableRow>
                        <TableCell component="th" scope="row">
                          {bus.busName}{" "}
                        </TableCell>
                        <TableCell align="center">{bus.busNo}</TableCell>
                        <TableCell align="center">{bus.driverName}</TableCell>
                        <TableCell align="center">{bus.source}</TableCell>
                        <TableCell align="center">{bus.destination}</TableCell>
                        <TableCell align="center">{bus.farePerSeat}</TableCell>
                        <TableCell align="center">{bus.time}</TableCell>
                        <TableCell align="center">
                          <Link href="/signin" >
                          {/* <Link to={"/signin"} > */}
                            <Button
                              color="secondary"  
                            >
                              BOOK
                            </Button>
                          </Link>{" "}
                        </TableCell>{" "}
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            {/* <Grid>
              <ul>
                {this.state.persons.map((bus) => (
                  <li>{bus}</li>
                ))}
              </ul>
            </Grid> */}
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default SearchBus;
