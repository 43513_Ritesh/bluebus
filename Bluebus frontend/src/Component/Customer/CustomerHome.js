import { Button, Grid } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import React from 'react'
import Image1 from '../../img/bus.jpg'
import Image2 from '../../img/car.jpg'

class CustomerHome extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const busStyle = {
      padding: 30,
      height: '60vh',
      width: 550,
      margin: '30px 70px',
      // backgroundImage: `url(${Image})`,
      background: 'none',
      backdropFilter: 'blur(10px)',
    }

    const carStyle = {
      padding: 30,
      height: '60vh',
      width: 550,
      margin: '-525px 750px',
      background: 'none',
      backdropFilter: 'blur(10px)',
    }

    const busimg = {
      backgroundImage: `url(${Image1})`,
      height: '40vh',
      width: 550,
    }

    const carimg = {
      backgroundImage: `url(${Image2})`,
      height: '40vh',
      width: 550,
    }

    const text = {}

    return (
      <Grid>
        <h1 style={text} align="center">
          Welcome To BlueBusAndCars
        </h1>
        <Paper elevation={10} style={busStyle}>
          <h3 align="center">Travel To Your Faviourate Destination!!!</h3>
          <Grid style={busimg}></Grid>
        </Paper>

        <Paper elevation={10} style={carStyle}>
          <h3 align="center">Enjoy Your Road Trips with Us!!!</h3>
          <Grid style={carimg}></Grid>
        </Paper>
      </Grid>
    )
  }
}

export default CustomerHome
