import { Avatar, Button, Grid, TextField } from '@material-ui/core'
import React from 'react'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

class MyTrips extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const avatarStyle = {
      backgroundColor: '#ff5252',
    }

    const tabStyle = {
      padding: 8,
    }

    const paperStyle = {
      padding: 30,
      height: '60vh',
      width: 1100,
      margin: '0px auto',
      backgroundColor: '#ffcd38',
    }
    const btnStyle = {
      padding: 10,
      margin: '40px 200px 0px 150px',
      width: 200,
      height: 60,
      backgroundColor: '#6fbf73',
    }

    const serachStyle = {
      margin: '20px 0px 0px 50px',
    }

    const searchbtn = {
      margin: '20px 0px 0px 50px',
    }

    const tableStyle = {
      margin: '40px 0px 0px 0px',
      backgroundColor: 'd1e0e0',
    }

    function createData(bus_name, bus_no, source, destination, Fare, Time) {
      return {
        bus_name,
        bus_no,
        source,
        destination,
        Fare,
        Time,
      }
    }

    const rows = [
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
      createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    ]

    return (
      <Grid>
        <Grid>
          <Paper elevation={10} style={paperStyle}>
            <Grid align="center">
              <TextField
                style={serachStyle}
                label="Source"
                variant="outlined"
                color="primary">
                Source
              </TextField>
              <TextField
                style={serachStyle}
                label="Destination"
                variant="outlined"
                color="primary">
                Destination
              </TextField>
              <TextField
                style={serachStyle}
                type="date"
                label="date"
                defaultValue="2000-01-01"
                variant="filled"
                color="primary"></TextField>
            </Grid>

            <Grid align="center">
              <Button
                onClick={this.search}
                style={searchbtn}
                variant="contained"
                color="#6fbf73">
                SEACRH
              </Button>
            </Grid>
            <Grid>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">bus_name</TableCell>
                      <TableCell align="center">bus_no</TableCell>
                      <TableCell align="center">source</TableCell>
                      <TableCell align="center">destination</TableCell>
                      <TableCell align="center">Fare_per_seat</TableCell>
                      <TableCell align="center">Time</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => (
                      <TableRow key={row.name}>
                        <TableCell component="th" scope="row">
                          {row.bus_name}{' '}
                        </TableCell>
                        <TableCell align="center">{row.bus_no}</TableCell>
                        <TableCell align="center">{row.source}</TableCell>
                        <TableCell align="center">{row.destination}</TableCell>

                        <TableCell align="center">
                          {row.Fare_per_seat}
                        </TableCell>
                        <TableCell align="center">{row.Time}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Paper>
        </Grid>
        <Grid></Grid>
      </Grid>
    )
  }
}

export default MyTrips
