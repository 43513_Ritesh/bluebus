import { Button, Grid, Paper } from '@material-ui/core'
import React from 'react'
import LogOut from '../LogOut'

class MyTrips extends React.Component {
  constructor(props) {
    super(props)
    this.info = this.info.bind(this)
  }


  info = (e) => {
    return <personalInfo />
  }

  render() {
    const paper = {
      height: '50vh',
      width: 300,
      margin: '50px 110px',
      background: 'none',
      backdropFilter: 'blur(10px)',
    }

    const personal = {
      width: 100,
      margin: '70px 92px',
    }

    return (
      <Paper style={paper} elevation={20}>
        <Button onClick={this.info} style={personal} color="secondary"
            variant="outlined">Personal Info</Button>
        <LogOut />
      </Paper>
    )
  }
}

export default MyTrips
