import {
    CssBaseline,
    Grid,
    Paper,
    TextField,
    Button,
    Link,
  } from "@material-ui/core";
  import React from "react";
  import UserService from "../Service/UserService";
  import Image from "../img/bg.jpg";
  import Table from "@material-ui/core/Table";
  import TableBody from "@material-ui/core/TableBody";
  import TableCell from "@material-ui/core/TableCell";
  import TableContainer from "@material-ui/core/TableContainer";
  import TableHead from "@material-ui/core/TableHead";
  import TableRow from "@material-ui/core/TableRow";
  
  class Transaction extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        startDate: "",
        endDate: "",
        kms: "",
        booking: {
          bookingId : 0,
          bookingDate : "",
          payment : 0.0,
          kms : 0.0,
          days : 0
        }
      };
      this.handlekms = this.handlekms.bind(this);
      this.handleStartDate = this.handleStartDate.bind(this);
      this.handleEndDate = this.handleEndDate.bind(this);
    }
  
   
  
    handlekms = (e) => this.setState({ kms: e.target.value });
    handleStartDate = (e) => this.setState({ startDate: e.target.value });
    handleEndDate = (e) => this.setState({ endDate: e.target.value });
  
    render() {
      const tabStyle = {
        padding: 40,
        height: "70vh",
        width: 340,
        margin: "0px auto",
      };
  
      const buttonStyle = {
        margin: "20px auto",
      };
  
      const avatarStyle = {
        backgroundColor: "#4db2b5",
      };
  
      const bgStyle = {
        backgroundImage: `url(${Image})`,
        minHeight: "100vh",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      };
  
      const paperStyle = {
        margin: "0px auto",
        
      };
      const ownerbtn = {
        backgroundColor: "",
      };
  
      const tableStyle = {
        margin: "20px auto",
        width : 700,
        background: "none",
        backdropFilter: "blur(6px)",
        
      }
  
      return (
        <Grid style={bgStyle}>
          <CssBaseline />
          <Grid style={paperStyle}>
            <Paper elevation={20} style={tabStyle} align="center">
              <h2>Enter Your Card Details</h2>
              <form>
                <TextField label="Card No" placeholder="xxxx xxxx xxxx xxxx" onChange={this.handlekms} fullWidth />
                <TextField
                  defaultValue="2000-01-01"
                  label="Exp Date"
                  onChange={this.handleStartDate}
                  fullWidth
                />
                <TextField
                  label="CVV"
                  placeholder="***"
                  onChange={this.handleEndDate}
                  fullWidth
                />
              </form>
              
               <Link href="/signin">
               <Button
                  style={buttonStyle}
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  PAY
                </Button></Link> 
            </Paper>
          </Grid>
        </Grid>
      );
    }
  }
  
  export default Transaction;
  