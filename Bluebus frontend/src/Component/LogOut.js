import { Button, Grid, Link } from '@material-ui/core'
import React from 'react'

class LogOut extends React.Component {
  constructor(props) {
    super(props)
    this.logout=this.logout.bind(this)
    //this.logout = this.logout.bind(this)
  }

  // logout = (e) => {
  //   this.props.history.push('/signin')
  // }

  logout = (e) => {
    window.localStorage.clear()
  }

  render() {
    const btn = {
      margin: '20px 92px',
    }

    return (
      <Grid>
        <Link href="/signin">
          <Button
          onClick={this.logout}
            color="secondary"
            variant="outlined"
            // onClick={this.logout}
            style={btn}>
            LOGOUT
          </Button>
        </Link>
      </Grid>
    )
  }
}

export default LogOut
