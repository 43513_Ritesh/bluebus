import { Grid } from '@material-ui/core'
import React from 'react'
import AdminNavbar from '../Navbar/AdminNavbar'

class Home extends React.Component {
  constructor(props) {
    super(props)
    // this.search = this.search.bind(this)
  }
  render() {
    const bgStyle = {
      // backgroundImage: `url(${Image})`,
      height: '98vh',
      margin: '0px auto',
    }

    return (
      <Grid style={bgStyle}>
        <h2>WELCOME ADMIN</h2>
      </Grid>
    )
  }
}

export default Home
