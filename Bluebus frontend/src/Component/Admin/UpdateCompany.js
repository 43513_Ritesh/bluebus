import {
  Avatar,
  Button,
  Grid,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core'
import React from 'react'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import DeleteSweepOutlinedIcon from '@material-ui/icons/DeleteSweepOutlined'
import UserService from '../../Service/UserService'

class UpdateCompany extends React.Component {
  constructor(props) {
    super(props)
    // company_id
    // bus_service
    // company_name
    // email
    // owner
    // password
    // phone
    // registrationNo
    this.state = {
      companyName: '',
      email: '',
      owner: '',
      password: '',
      phone: '',
      registrationNo: '',
    }
    this.addOwner = this.addOwner.bind(this)
    this.handleCompany = this.handleCompany.bind(this)
    this.handleEmail = this.handleEmail.bind(this)
  }

  addOwner = (e) => {
    e.preventDefault()
    let user = {
      companyName: this.state.companyName,
      owner: this.state.owner,
      email: this.state.email,
      password: this.state.password,
      registrationNo: this.state.registrationNo,
      phone: this.state.phone,
    }
    UserService.addOwner(user).then((res) => {
      alert("Company Added Successfully !!!")
      //this.props.history.push('/adminnavbar')
    })
  }

  handleCompany = (e) => {
    console.log('in handler')
    this.setState({ companyName: e.target.value })
    console.log(this.state.companyName)
  }
  handleOwner = (e) => {
    this.setState({ owner: e.target.value })
  }
  handleEmail = (e) => {
    this.setState({ email: e.target.value })
  }
  handlePassword = (e) => {
    this.setState({ password: e.target.value })
  }
  handleRegistration = (e) => {
    this.setState({ registrationNo: e.target.value })
  }
  handlePhone = (e) => {
    this.setState({ phone: e.target.value })
  }

  render() {
    const paperStyle = {
      height: '90vh',
      width: 700,
      margin: '60px auto',
    }

    const buttonStyle = {
      margin: '40px auto',
    }

    const avatarStyle = {
      backgroundColor: '#000000',
    }

    const tabStyle = {
      margin: '20px auto',
      width: 450,
      padding: 8,
      backgroundColor: '#99ffd6',
      background: 'none',
      backdropFilter: 'blur(10px)',
    }

    return (
      <Grid>
        <Grid style={paperStyle}>
          <Paper elevation={10} style={tabStyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
            <h2>UDATE COMPANY</h2>
            <form>
              <TextField
                onChange={this.handleCompany}
                label="Company Name"
                fullWidth
              />
              <TextField onChange={this.handleOwner} label="Owner" fullWidth />
              <TextField onChange={this.handleEmail} label="Email" fullWidth />
              <TextField
                onChange={this.handlePassword}
                label="Password"
                type="password"
                fullWidth
              />
              <TextField onChange={this.handlePhone} label="Phone" fullWidth />
              <TextField
                onChange={this.handleRegistration}
                label="Registraion No."
                fullWidth
              />
            </form>
            <Button
              style={buttonStyle}
              variant="contained"
              color="primary"
              onClick={this.addOwner}>
              ADD
            </Button>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

export default UpdateCompany
