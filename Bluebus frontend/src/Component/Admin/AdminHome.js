import { Avatar, Button, Grid, Paper, TextField } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React from "react";
import UserService from "../../Service/UserService";

class AdminHome extends React.Component {
  constructor(props) {
    super(props);
    // company_id
    // bus_service
    // company_name
    // email
    // owner
    // password
    // phone
    // registrationNo
    this.state = {
      companyName: "",
      email: "",
      owner: "",
      password: "",
      phone: "",
      registrationNo: "",
      busService: 0,
      carService: 0,
    };
    this.addOwner = this.addOwner.bind(this);
    this.handleCompany = this.handleCompany.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleChangebus = this.handleChangebus.bind(this);
    this.handleChangecar = this.handleChangecar.bind(this);
  }

  addOwner = (e) => {
    if (
      this.state.companyName === "" ||
      this.state.owner === "" ||
      this.state.email === "" ||
      this.state.password === "" ||
      this.state.registrationNo === "" ||
      this.state.phone === ""
    ) {
      alert("All fields required");
    } else {
      e.preventDefault();
      let user = {
        companyName: this.state.companyName,
        owner: this.state.owner,
        email: this.state.email,
        password: this.state.password,
        registrationNo: this.state.registrationNo,
        phone: this.state.phone,
        busService: this.state.busService,
        carService: this.state.carService,
      };
      console.log(user);
      UserService.addOwner(user).then((res) => {
        // this.props.history.push('/adminnavbar')
        alert("Company added successfully");
      });
    }
  };

  handleCompany = (e) => {
    console.log("in handler");
    this.setState({ companyName: e.target.value });
    console.log(this.state.companyName);
  };
  handleOwner = (e) => {
    this.setState({ owner: e.target.value });
  };
  handleEmail = (e) => {
    this.setState({ email: e.target.value });
  };
  handlePassword = (e) => {
    this.setState({ password: e.target.value });
  };
  handleRegistration = (e) => {
    this.setState({ registrationNo: e.target.value });
  };
  handlePhone = (e) => {
    this.setState({ phone: e.target.value });
  };
  handleChangebus = (e) => {
    this.setState({ bsuService: 1 });
  };
  handleChangecar = (e) => {
    this.setState({ carService: 1 });
  };

  render() {
    const paperStyle = {
      height: "90vh",
      width: 700,
      margin: "60px auto",
    };

    const buttonStyle = {
      margin: "40px auto",
    };

    const avatarStyle = {
      backgroundColor: "#000000",
    };

    const tabStyle = {
      margin: "20px auto",
      width: 450,
      padding: 8,
      backgroundColor: "#99ffd6",
      background: "none",
      backdropFilter: "blur(10px)",
    };

    return (
      <Grid>
        <Grid style={paperStyle}>
          <Paper elevation={10} style={tabStyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
            <h2>ADD COMPANY</h2>
            <form>
              <TextField
                onChange={this.handleCompany}
                label="Company Name"
                fullWidth
              />
              <TextField onChange={this.handleOwner} label="Owner" fullWidth />
              <TextField onChange={this.handleEmail} label="Email" fullWidth />
              <TextField
                onChange={this.handlePassword}
                label="Password"
                type="password"
                fullWidth
              />
              <TextField onChange={this.handlePhone} label="Phone" fullWidth />
              <TextField
                onChange={this.handleRegistration}
                label="Registraion No."
                fullWidth
              />
              BUS
              <Checkbox
                label="BUS"
                onChange={this.handleChangebus}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              CAR
              <Checkbox
                label="CAR"
                onChange={this.handleChangecar}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
            </form>
            <Button
              style={buttonStyle}
              variant="contained"
              color="primary"
              onClick={this.addOwner}
            >
              ADD
            </Button>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default AdminHome;
