import { Button, Grid,Link } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import React from "react";
import UserService from "../../Service/UserService";

class AllCompanies extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      companies: [],
    };

    this.search = this.search.bind(this);
  }

  search = (e) => {
    e.preventDefault();
    UserService.getCompanies().then((res) => {
      const companies = res.data;
      this.setState({ companies });
      // this.props.history.push('/adminnavbar')
    });
  };

  render() {
    const avatarStyle = {
      backgroundColor: "#ff5252",
    };

    const tabStyle = {
      padding: 8,
    };

    const paperStyle = {
      padding: 30,
      height: "100vh",
      width: 1100,
      margin: "0px auto",
      background: "none",
      backdropFilter: "blur(10px)",
    };
    const btnStyle = {
      padding: 10,
      margin: "40px 200px 0px 150px",
      width: 200,
      height: 60,
      backgroundColor: "#6fbf73",
    };

    const serachStyle = {
      margin: "20px 0px 0px 50px",
    };

    const searchbtn = {
      margin: "20px 0px 0px 50px",
    };

    const tableStyle = {
      margin: "40px 0px 0px 0px",
      background: "none",
    };

    // function createData(bus_name, bus_no, source, destination, Fare, Time) {
    //   return {
    //     bus_name,
    //     bus_no,
    //     source,
    //     destination,
    //     Fare,
    //     Time,
    //   }
    // }

    // const buses = [
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    // ]

    return (
      <Grid>
        <Grid>
          <Paper elevation={10} style={paperStyle}>
            <Button color="primary" variant="filled" onClick={this.search}>SHOW ALL</Button>
            <Grid align="center"></Grid>
            <Grid align="center"></Grid>
            <Grid>

              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">Company Name</TableCell>
                      <TableCell align="center">Email</TableCell>
                      <TableCell align="center">Owner</TableCell>
                      <TableCell align="center">Phone</TableCell>
                      <TableCell align="center">Registration No</TableCell>
                      <TableCell align="center">Update</TableCell>
                      <TableCell align="center">Delete</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.companies.map((company) => (
                      <TableRow>
                        <TableCell component="th" scope="row">
                          {company.companyName}{" "}
                        </TableCell>
                        <TableCell align="center">{company.email}</TableCell>
                        <TableCell align="center">{company.owner}</TableCell>
                        <TableCell align="center">{company.phone}</TableCell>
                        <TableCell align="center">
                          {company.registrationNo}
                        </TableCell>
                        <TableCell align="center">
                          <Link href="/updatecompany"><Button color="primary">UDATE</Button></Link>
                        </TableCell>
                        <TableCell align="center">
                          <Button color="secondary">DELETE</Button>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            {/* <Grid>
              <ul>
                {this.state.persons.map((bus) => (
                  <li>{bus}</li>
                ))}
              </ul>
            </Grid> */}
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default AllCompanies;
