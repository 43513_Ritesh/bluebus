import { Button, Grid,Link } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import React from "react";
import UserService from '../../../Service/UserService'

class AllCars extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cars: [],
    };

    this.search = this.search.bind(this);
  }

  search = (e) => {
    e.preventDefault();
    UserService.getCarByOwner().then((res) => {
      const cars = res.data;
      this.setState({ cars });
      // this.props.history.push('/adminnavbar')
    });
  };

  render() {
    const avatarStyle = {
      backgroundColor: "#ff5252",
    };

    const tabStyle = {
      padding: 8,
    };

    const paperStyle = {
      padding: 30,
      height: "100vh",
      width: 1100,
      margin: "0px auto",
      background: "none",
      backdropFilter: "blur(10px)",
    };
    const btnStyle = {
      padding: 10,
      margin: "40px 200px 0px 150px",
      width: 200,
      height: 60,
      backgroundColor: "#6fbf73",
    };

    const serachStyle = {
      margin: "20px 0px 0px 50px",
    };

    const searchbtn = {
      margin: "20px 0px 0px 50px",
    };

    const tableStyle = {
      margin: "40px 0px 0px 0px",
      background: "none",
    };

    // function createData(bus_name, bus_no, source, destination, Fare, Time) {
    //   return {
    //     bus_name,
    //     bus_no,
    //     source,
    //     destination,
    //     Fare,
    //     Time,
    //   }
    // }

    // const buses = [
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    //   createData('Shivshahi', 'MH15-BN-3306', 'Nashik', 'Pune', 530, '10AM'),
    // ]

    return (
      <Grid>
        <Grid>
          <Paper elevation={10} style={paperStyle}>
            <Button color="primary" variant="filled" onClick={this.search}>SHOW ALL</Button>
            <Grid align="center"></Grid>
            <Grid align="center"></Grid>
            <Grid>

              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                  <TableRow>
                     <TableCell align="center">Car Name</TableCell>
                      <TableCell align="center">Car No</TableCell>
                      <TableCell align="center">City</TableCell>
                      <TableCell align="center">Price Per Day</TableCell>
                      <TableCell align="center">Rs Per Km</TableCell>
                      <TableCell align="center">Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.cars.map((car) => (
                      <TableRow>
                         <Link href="#"><TableCell component="th" scope="row">
                          {car.carName}{" "}
                        </TableCell></Link>
                        <TableCell align="center">{car.carNo}</TableCell>
                        <TableCell align="center">{car.city}</TableCell>
                        <TableCell align="center">{car.pricePerDay}</TableCell>
                        <TableCell align="center">{car.rsPerKm}</TableCell>
                        <TableCell align="center"><Link href="/signin?car.carId=@car.carId"><Button color="secondary">BOOK</Button></Link> </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            {/* <Grid>
              <ul>
                {this.state.persons.map((bus) => (
                  <li>{bus}</li>
                ))}
              </ul>
            </Grid> */}
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default AllCars;
