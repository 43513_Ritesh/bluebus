import { Avatar, Button, Grid, Paper, TextField } from '@material-ui/core'
import React from 'react'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import UserServices from '../../../Service/UserService'
class AddCar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      carName: '',
      carNo: '',
      city: '',
      pricePerDay: 0.0,
      rsPerKm: 0.0,
    }
    this.addcar = this.addcar.bind(this)
  }

  addcar = (e) => {
    e.preventDefault()
    if (window.localStorage.getItem("comcarService") == "true") {
    if (
      this.state.carName === '' ||
      this.state.carNo === '' ||
      this.state.city === '' ||
      this.state.pricePerDay === '' ||
      this.state.rsPerKm === ''
    ) {
      alert('All fields required')
    } else {
      console.log('addcar!!!!!')
      let car = {
        carName: this.state.carName,
        carNo: this.state.carNo,
        city: this.state.city,
        pricePerDay: this.state.pricePerDay,
        rsPerKm: this.state.rsPerKm,
      }

      UserServices.addCarByOwner(car).then((res) => {
        console.log(res)
        if (res.data === 'Car added successfully') {
          alert('Car added successfully')
          this.props.history.push('/ownerhome')
        } else {
          alert('Car registration failed')
        }
      })
    }
    }else {
      alert("You don't have access to bus services");
    }
  }
  handleChangecarName = (e) => this.setState({ carName: e.target.value })
  handleChangecarNo = (e) => this.setState({ carNo: e.target.value })
  handleChangecity = (e) => this.setState({ city: e.target.value })
  handleChangepricePerDay = (e) =>
    this.setState({ pricePerDay: e.target.value })
  handleChangersPerKm = (e) => this.setState({ rsPerKm: e.target.value })
  buses = (e) => {
    this.props.history.push('/allbuses')
  }
  addBus = (e) => {
    this.props.history.push('/addbus')
  }
  ownerHome = (e) => {
    this.props.history.push('/ownerhome')
  }
  addCar = (e) => {
    this.props.history.push('/addcar')
  }

  render() {
    const navStyle = {
      backgroundColor: '#99ffd6',
    }

    const buttonStyle = {
      margin: '20px auto',
    }

    const avatarStyle = {
      backgroundColor: '#000000',
    }

    const tabStyle = {
      padding: 20,
      backgroundColor: '#99ffd6',
      width: 500,
      margin: '20px auto',
      background: 'none',
      backdropFilter: 'blur(6px)',
    }
    return (
      <Grid align="center">
        <Paper elevation={10} style={tabStyle} align="center">
          <Avatar style={avatarStyle}>
            <LockOutlinedIcon />
          </Avatar>
          <h2>Add Car</h2>
          <form>
            <TextField
              label="Car Name"
              onChange={this.handleChangecarName}
              fullWidth
            />
            <TextField
              label="Car No"
              onChange={this.handleChangecarNo}
              fullWidth
            />
            <TextField
              label="City"
              onChange={this.handleChangecity}
              fullWidth
            />
            <TextField
              label="Price Per Day"
              type="number"
              onChange={this.handleChangepricePerDay}
              fullWidth
            />

            <TextField
              label="Rs Per Km"
              type="number"
              onChange={this.handleChangersPerKm}
              fullWidth
            />
          </form>
          <Button
            onClick={this.addcar}
            style={buttonStyle}
            variant="contained"
            color="primary"
            fullWidth>
            Add Car
          </Button>

          {/* <button
            style={buttonStyle}
            variant="contained"
            color="primary"
            fullWidth>
            Clear data
          </button> */}
        </Paper>
      </Grid>
    )
  }
}

export default AddCar
