import UserServices from "../../Service/UserService";
import {
  Avatar,
  Button,
  Grid,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Link from "@material-ui/core/Link";
import Image from "../../img/bg.jpg";

// import { EmojiFlagsRounded } from '@material-ui/icons'

class OwnerSignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    this.signin = this.signin.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  signin = (e) => {
    e.preventDefault();
    window.localStorage.clear();
    let user = {
      email: this.state.email,
      password: this.state.password,
    };
    console.log("in signin");
    UserServices.signInOwner(user).then((res) => {
      let u = res.data;
      window.localStorage.setItem("comId", u.companyId);
      window.localStorage.setItem("comname", u.companyName);
      window.localStorage.setItem("comowner", u.owner);
      window.localStorage.setItem("compassword", u.password);
      window.localStorage.setItem("comregistrationNo", u.registrationNo);
      window.localStorage.setItem("combusService", u.busService);
      window.localStorage.setItem("comcarService", u.carService);
      window.localStorage.setItem("owner", u.owner);
      alert(
        "CompanyId : " +
          window.localStorage.getItem("comId") +
          "  Name : " +
          window.localStorage.getItem("comname") +
          "  Owner : " +
          window.localStorage.getItem("owner")
      );

      this.props.history.push("/ownernavbar");
    });
  };

  handleChange = (e) => this.setState({ email: e.target.value });
  handleChange2 = (e) => this.setState({ password: e.target.value });

  render() {
    const tabStyle = {
      padding: 30,
      height: "70vh",
      width: 280,
      margin: "0px 1050px",
    };

    const buttonStyle = {
      margin: "70px auto",
    };

    const avatarStyle = {
      backgroundColor: "#4db2b5",
    };

    const bgStyle = {
      backgroundImage: `url(${Image})`,
      minHeight: "100vh",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    };

    const paperStyle = {
      margin: "0px auto",
    };

    return (
      <Grid style={bgStyle}>
        <Grid style={paperStyle}>
          <Paper elevation={10} style={tabStyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
            <h2>Owner Login !</h2>
            <form>
              <TextField label="Email" onChange={this.handleChange} fullWidth />
              <TextField
                label="Password"
                type="password"
                onChange={this.handleChange2}
                fullWidth
              />
            </form>
            <Link href="/customernavbar">
              <Button
                onClick={this.signin}
                style={buttonStyle}
                variant="contained"
                color="primary"
                fullWidth
              >
                Sign In
              </Button>
            </Link>
            <Typography>
              Customer <Link href="/signin">SignIn</Link>
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default OwnerSignIn;
