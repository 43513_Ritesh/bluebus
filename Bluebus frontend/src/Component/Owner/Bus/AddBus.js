import { Avatar, Button, Grid, Paper, TextField } from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React from "react";
import UserServices from "../../../Service/UserService";

class Addbus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busName: "",
      busNo: "",
      driverName: "",
      source: "",
      destination: "",
      time: "",
      farePerSeat: 0.0,
    };
    this.buses = this.buses.bind(this);
    this.addBus = this.addBus.bind(this);
    this.addCar = this.addCar.bind(this);
    this.ownerHome = this.ownerHome.bind(this);
  }
  buses = (e) => {
    this.props.history.push("/allbuses");
  };
  ownerHome = (e) => {
    console.log(window.localStorage.getItem("combusService"));
    this.props.history.push("/ownerhome");
  };

  addCar = (e) => {
    if (window.localStorage.getItem("comcarService") == "true") {
      this.props.history.push("/addcar");
    } else {
      alert("You have not added car service");
    }
  };

  addbus = (e) => {
    e.preventDefault();
    if (window.localStorage.getItem("combusService") == "true") {
      {
        if (
          this.state.busNo === "" ||
          this.state.busName === "" ||
          this.state.destination === "" ||
          this.state.driverName === "" ||
          this.state.farePerSeat === "" ||
          this.state.source === "" ||
          this.state.time === ""
        ) {
          alert("All fields required");
        } else {
          console.log("addbus!!!!!");
          let bus = {
            busName: this.state.busName,
            busNo: this.state.busNo,
            driverName: this.state.driverName,
            source: this.state.source,
            destination: this.state.destination,
            time: this.state.time,
            farePerSeat: this.state.farePerSeat,
          };
          UserServices.addBusByOwner(bus).then((res) => {
            console.log(res);
            alert(res.data);
            if (res.data === "Bus added successfully") {
              alert("Bus added successfully");
              this.props.history.push("/ownerhome");
            } else {
              alert("Bus registration failed");
            }
          });
        }
      }
    } else {
      alert("You don't have access to bus services");
    }
  };
  handleChangeBusName = (e) => this.setState({ busName: e.target.value });
  handleChangeBusNo = (e) => this.setState({ busNo: e.target.value });
  handleChangeDriverName = (e) => this.setState({ driverName: e.target.value });
  handleChangeSource = (e) => this.setState({ source: e.target.value });
  handleChangeDestination = (e) =>
    this.setState({ destination: e.target.value });
  handleChangeBusTime = (e) => this.setState({ time: e.target.value });
  handleChangeFarePerSeat = (e) =>
    this.setState({ farePerSeat: e.target.value });
  buses = (e) => {
    this.props.history.push("/allbuses");
  };
  addBus = (e) => {
    this.props.history.push("/addbus");
  };
  ownerHome = (e) => {
    this.props.history.push("/ownerhome");
  };
  addCar = (e) => {
    this.props.history.push("/addcar");
  };

  render() {
    const navStyle = {
      backgroundColor: "#99ffd6",
    };

    const buttonStyle = {
      margin: "20px auto",
    };

    const avatarStyle = {
      backgroundColor: "#000000",
    };

    const tabStyle = {
      padding: 20,
      backgroundColor: "#99ffd6",
      width: 500,
      margin: "20px auto",
      background: "none",
      backdropFilter: "blur(6px)",
    };

    return (
      <Grid align="center">
        <Paper elevation={10} style={tabStyle} align="center">
          <Avatar style={avatarStyle}>
            <LockOutlinedIcon />
          </Avatar>
          <h2>Add Bus</h2>
          <form>
            <TextField
              label="Bus Name"
              onChange={this.handleChangeBusName}
              fullWidth
              autoComplete="on"
            />
            <TextField
              label="Bus No"
              onChange={this.handleChangeBusNo}
              fullWidth
            />

            <TextField
              label="Driver Name"
              onChange={this.handleChangeDriverName}
              fullWidth
            />
            <TextField
              label="Source"
              onChange={this.handleChangeSource}
              fullWidth
            />
            <TextField
              label="Destination"
              onChange={this.handleChangeDestination}
              fullWidth
            />

            <TextField
              id="time"
              label="Bus Time"
              type="time"
              placeholder="Click on clock tab to select time"
              onChange={this.handleChangeBusTime}
              fullWidth
            />
            <TextField
              label="Fare Per Seat"
              type="number"
              onChange={this.handleChangeFarePerSeat}
              fullWidth
            />
          </form>
          <Button
            onClick={this.addbus}
            style={buttonStyle}
            variant="contained"
            color="primary"
            fullWidth
          >
            Add Bus
          </Button>

          {/* <button
            style={buttonStyle}
            variant="contained"
            color="primary"
            fullWidth>
            Clear data
          </button> */}
        </Paper>
      </Grid>
    );
  }
}

export default Addbus;
