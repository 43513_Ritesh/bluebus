import { Avatar, Grid, Paper, Tab, Tabs } from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import React from 'react'
import { Link } from 'react-router-dom'
import Ownernav from './include/OwnerNav'

class OwnerHome extends React.Component {
  constructor(props) {
    super(props)
    this.buses = this.buses.bind(this)
    this.addBus = this.addBus.bind(this)
    this.addCar = this.addCar.bind(this)
    this.ownerHome = this.ownerHome.bind(this)
  }

  buses = (e) => {
    this.props.history.push('/allbuses')
  }
  addBus = (e) => {
    if (window.localStorage.getItem('combusService') == 'true') {
      this.props.history.push('/addbus')
    } else {
      alert('You have not added bus service')
    }
  }
  ownerHome = (e) => {
    console.log(window.localStorage.getItem('combusService'))
    this.props.history.push('/ownerhome')
  }
  addCar = (e) => {
    if (window.localStorage.getItem('comcarService') == 'true') {
      this.props.history.push('/addcar')
    } else {
      alert('You have not added car service')
    }
  }
  render() {
    const bgStyle = {
      // backgroundImage: `url(${Image})`,
      height: '98vh',
      margin: '0px auto',
    }
    const paperStyle = {
      margin: '0px auto',
    }

    // const buttonStyle = {
    //   margin: "20px auto",
    // };

    const avatarStyle = {
      backgroundColor: '#000000',
    }

    const tabStyle = {
      padding: 8,
      backgroundColor: '#99ffd6',
    }
    const navStyle = {
      backgroundColor: '#99ffd6',
    }
    return (
      <Grid style={bgStyle}>
        {/* <Ownernav {...this.state} /> */}
        <Grid>
          <Paper elevation={10} style={navStyle}>
            <Tabs
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary">
              <Tab label="Home" onClick={this.ownerHome} />
              <Tab label="Add Bus" onClick={this.addBus} />
              <Tab label="Add Car" onClick={this.addCar} />
              <Tab label="Buses" onClick={this.buses} />
              <Tab label="Cars" />
              <Tab label="Add Bookings" />
              <Tab label="About Us" />
              <Tab label="Contact Us" />
              <Link href="showbuses"></Link>
            </Tabs>
          </Paper>
        </Grid>
        <Grid style={paperStyle}>
          <Paper elevation={10} style={tabStyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

export default OwnerHome
