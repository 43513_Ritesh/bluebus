import { Grid, Paper, Tab, Tabs } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";


class Ownernav extends React.Component {
    constructor(props) {
        super(props);
        this.buses = this.buses.bind(this);
        this.addBus = this.addBus.bind(this);
        this.addCar = this.addCar.bind(this);
        this.ownerHome = this.ownerHome.bind(this);        
      }
 

   buses =(e)=> {
    this.props.history.push("/allbuses");
  }
   addBus =(e)=> {
    if (window.localStorage.getItem("combusService") == "true") {
      this.props.history.push("/addbus");
    } else {
      alert("You have not added bus service");
    }
  }
   ownerHome =(e)=> {
       console.log(window.localStorage.getItem("combusService"))
    this.props.history.push("/ownerhome");
  }
   addCar =(e)=> {
    if (window.localStorage.getItem("comcarService") == "true") {
      this.props.history.push("/addcar");
    } else {
      alert("You have not added car service");
    }
  }
 
  render() {
    const navStyle = {
        backgroundColor: "#99ffd6",
      };
    return (
    <Grid>
      <Paper elevation={10} style={navStyle}>
        <Tabs>
          <Tab label="Home" onClick={this.ownerHome} />
          <Tab label="Add Bus" onClick={this.addBus} />
          <Tab label="Add Car" onClick={this.addCar} />
          <Tab label="Buses" onClick={this.buses} />
          <Tab label="Cars" />
          <Tab label="Add Bookings" />
          <Tab label="About Us" />
          <Tab label="Contact Us" />
          <Link href="showbuses"></Link>
        </Tabs>
      </Paper>
    </Grid>
  );
};
}
export default Ownernav;
