import { Avatar, Button, Grid, TextField,Box, Typography } from "@material-ui/core";
import React from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import adImage from "../img/1.jpg";
import Image from "../img/map.jpg"

class ContactUs extends React.Component {
  constructor(props) {
    super(props);
  }
  CustomerHome=(e)=>{
    this.props.history.push("/CustomerHome")
  }

  SearchBus = (e) => {
    this.props.history.push("/SearchBus");
  };

  SearchCar = (e) => {
    this.props.history.push("/SearchCar");
  };
  MyTrips = (e) => {
    this.props.history.push("/MyTrips");
  };
  AboutUs = (e) => {
    this.props.history.push("/AboutUs");
  };
  ContactUs = (e) => {
    this.props.history.push("/ContactUs");
  };
  Profile = (e) => {
    this.props.history.push("/Profile");
  };

  render() {
    const bgStyle = {
      backgroundImage: `url(${adImage})`,
      height: "98vh",
      margin: "0px auto",
    };
    const avatarStyle = {
      backgroundColor: "#ff5252",
    };

    const tabStyle = {
      padding: 8,
      backgroundColor: "#99ffd6",
      
    };

    const mapSTyle={
      backgroundImage:`url(${Image})`,
      height:"70vh",
      margin: "0px auto",
    }
    const paperStyle = {
      padding: 30,
      height: "20vh",
      width: 1100,
      margin: "0px auto",
      // backgroundColor: "#ffcd38",
    };
  

    return (
      // <Grid style={bgStyle}>
      //   <Paper elevation={10}>
      //     <Tabs
      //       style={tabStyle}
      //       indicatorColor="secondary"
      //       textColor="secondary"
      //     >
      //       <Avatar style={avatarStyle}>
      //         <PersonOutlineOutlinedIcon />
      //       </Avatar>
      //       <Tab label="home" onClick={this.CustomerHome} />
      //       <Tab label="search bus" onClick={this.SearchBus} />
      //       <Tab label="search car" onClick={this.SearchCar} />
      //       <Tab label="my trips" onClick={this.MyTrips} />
      //       <Tab label="profile" onClick={this.Profile} />
      //       <Tab label="about us" onClick={this.AboutUs} />
      //       <Tab label="contact us" onClick={this.ContactUs} />
      //     </Tabs>
      //   </Paper>
        <Grid style={bgStyle}>
        <Grid style={paperStyle}>
          <Paper elevation={10} style={mapSTyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography variant="h3">Contact Us</Typography>
              <Typography style={{ margin: "20 px 0" }}>
                <Box p={3} textAlign="center">
                  <h1>Pune JRDA Group Pvt. Ltd , 5th
                  Floor, hadapsar , HAL 2nd Stage,other branches
                  swarget(pune), kothrud(pune),airport road(mumbai),rajarampuri(kolhapur)- 560008.
                  </h1>
                </Box>

                <Typography style={{ margin: "20 px 0" }}>
                  <Box>
                <h1> Ph: ++918407920626 For any Support or Complaints
                    Call Centre Time : 24*7 For Press enquiries, please send
                    email to JRDA@bluebus.com
                    </h1>
                  </Box>
                </Typography>
              </Typography>
            
          
          </Paper>
        </Grid>
      </Grid>
      // </Grid>
    );
  }
}

export default ContactUs;
