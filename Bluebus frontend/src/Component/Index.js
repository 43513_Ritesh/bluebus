import { Avatar, Button, Grid, TextField } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import Tab from '@material-ui/core/Tab'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Tabs from '@material-ui/core/Tabs'
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined'
import React from 'react'
// import Bus from './bus'

class Index extends React.Component {
  constructor(props) {
    super(props)
    // this.search = this.search.bind(this)
  }

  signin = () => {
    this.props.history.push('/signin')
  }

  signup = () => {
    this.props.history.push('/signup')
  }

  render() {
    const bgStyle = {
      // backgroundImage: `url(${Image})`,
      height: '98vh',
      margin: '0px auto',
    }

    const avatarStyle = {
      backgroundColor: '#000000',
    }

    const tabStyle = {
      padding: 8,
      backgroundColor: '#99ffd6',
    }

    const paperStyle = {
      padding: 30,
      height: '60vh',
      width: 1100,
      margin: '0px auto',
      backgroundColor: '#e6fffa',
    }
    const btnStyle = {
      padding: 10,
      margin: '40px 200px 0px 150px',
      width: 200,
      height: 60,
      backgroundColor: '#99ffd6',
    }

    const serachStyle = {
      margin: '20px 0px 0px 50px',
    }

    const searchbtn = {
      margin: '20px 0px 0px 50px',
    }

    function createData(
      bus_id,
      bus_name,
      bus_no,
      source,
      destination,
      Driver_name,
      Fare_per_seat,
      Time,
      Company_id
    ) {
      return {
        bus_id,
        bus_name,
        bus_no,
        source,
        destination,
        Driver_name,
        Fare_per_seat,
        Time,
        Company_id,
      }
    }

    const rows = [
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
      createData(
        1,
        'Shivshahi',
        'MH15-BN-3306',
        'Nashik',
        'Pune',
        'Ashok',
        530,
        '10AM',
        223344
      ),
    ]

    return (
      <Grid style={bgStyle}>
        <Paper elevation={10}>
          <Tabs
            style={tabStyle}
            indicatorColor="secondary"
            textColor="secondary">
            <Avatar style={avatarStyle}>
              <PersonOutlineOutlinedIcon />
            </Avatar>
            <Tab label="ABOUT US" />
            <Tab label="CONTACT US" />
            <Tab label="Sign In" onClick={this.signin} />
            <Tab label="Sign Up" onClick={this.signup} />
          </Tabs>
        </Paper>
        <Grid>
          <Grid align="center">
            <Button style={btnStyle}>BUS</Button>
            <Button style={btnStyle}>CAR</Button>
          </Grid>
          <Paper elevation={10} style={paperStyle}>
            <Grid align="center">
              <TextField
                style={serachStyle}
                label="Source"
                variant="outlined"
                color="primary">
                Source
              </TextField>
              <TextField
                style={serachStyle}
                label="Destination"
                variant="outlined"
                color="primary">
                Destination
              </TextField>
              <TextField
                style={serachStyle}
                type="date"
                label="date"
                defaultValue="2000-01-01"
                variant="filled"
                color="primary"></TextField>
            </Grid>
            <Grid align="center">
              <Button
                onClick={this.search}
                style={searchbtn}
                variant="contained"
                color="secondary">
                SEACRH
              </Button>
            </Grid>
            <Grid>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>bus_id</TableCell>
                      <TableCell align="center">bus_name</TableCell>
                      <TableCell align="center">bus_no</TableCell>
                      <TableCell align="center">source</TableCell>
                      <TableCell align="center">destination</TableCell>
                      <TableCell align="center">Driver_name</TableCell>
                      <TableCell align="center">Fare_per_seat</TableCell>
                      <TableCell align="center">Time</TableCell>
                      <TableCell align="center">Company_id</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => (
                      <TableRow key={row.name}>
                        <TableCell align="center">{row.bus_id}</TableCell>
                        <TableCell component="th" scope="row">
                          {row.bus_name}
                        </TableCell>
                        <TableCell align="center">{row.bus_no}</TableCell>
                        <TableCell align="center">{row.source}</TableCell>
                        <TableCell align="center">{row.destination}</TableCell>

                        <TableCell align="center">{row.Driver_name}</TableCell>
                        <TableCell align="center">
                          {row.Fare_per_seat}
                        </TableCell>
                        <TableCell align="center">{row.Time}</TableCell>
                        <TableCell align="center">{row.Company_id}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Paper>
        </Grid>
        <Grid></Grid>
      </Grid>
    )
  }
}

export default Index
