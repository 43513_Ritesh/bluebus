import { Avatar, Grid, Paper } from '@material-ui/core'
import React from 'react'
import InstagramIcon from '@material-ui/icons/Instagram'
import TwitterIcon from '@material-ui/icons/Twitter'
import FacebookIcon from '@material-ui/icons/Facebook'

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const paper = {
      backgroundColor: 'rgb(180, 180, 180)',
      height: '35vh',
    }
    const abt = {
      padding: 50,
    }
    const cnt = {
      padding: 50,
    }

    const insta = {
      backgroundColor: '#8a3ab9',
    }

    const twitter = {
      backgroundColor: '#1DA1F2',
    }

    const facebook = {
      backgroundColor: '#3b5998',
    }

    const ava = {
      margin: '20px auto',
    }

    return (
      <Grid>
        <Paper elevation={10} style={paper}>
          <p align="center">
            BlueBusAndCars is most effficient online bus ticketing & Rental Car
            Booking platform that has transformed bus travel & Rental Car in the
            country by bringing ease and convenience to millions of Indians who
            travel using buses & Cars Founded in 2021.
          </p>
          <Grid align="center" style={ava}>
            <Avatar style={insta}>
              <InstagramIcon />
            </Avatar>
            <Avatar style={twitter}>
              <TwitterIcon />
            </Avatar>
            <Avatar style={facebook}>
              <FacebookIcon />
            </Avatar>
          </Grid>
          {/* <Grid align="center">
            <a href="/aboutus" style={abt}>
              About Us
            </a>
            <a href="/contactus" style={cnt}>
              Contact Us
            </a>
          </Grid> */}

          <h3 align="center">Copyright@BlueBus 2021</h3>
        </Paper>
      </Grid>
    )
  }
}

export default Footer
