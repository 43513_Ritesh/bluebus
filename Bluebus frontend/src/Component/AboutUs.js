import {
  Avatar,
  Button,
  Grid,
  TextField,
  Box,
  Typography,
} from "@material-ui/core";
import React from "react";
import Paper from "@material-ui/core/Paper";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import adImage from "../img/1.jpg";

class AboutUs extends React.Component {
  constructor(props) {
    super(props);
  }

  CustomerHome = (e) => {
    this.props.history.push("/CustomerHome");
  };
  SearchBus = (e) => {
    this.props.history.push("/SearchBus");
  };

  SearchCar = (e) => {
    this.props.history.push("/SearchCar");
  };
  MyTrips = (e) => {
    this.props.history.push("/MyTrips");
  };
  AboutUs = (e) => {
    this.props.history.push("/AboutUs");
  };
  ContactUs = (e) => {
    this.props.history.push("/ContactUs");
  };
  Profile = (e) => {
    this.props.history.push("/Profile");
  };

  render() {
    const bgStyle = {
      backgroundImage: `url(${adImage})`,
      height: "98vh",
      margin: "0px auto",
    };
    const avatarStyle = {
      backgroundColor: "#ff5252",
    };

    const tabStyle = {
      padding: 8,
      backgroundColor: "#99ffd6",
    };

    const paperStyle = {
      padding: 30,
      height: "20vh",
      width: 1100,
      margin: "0px auto",
      // backgroundColor: "#ffcd38",
    };

    return (
      // <Grid style={bgStyle}>
      //   <Paper elevation={10}>
      //     <Tabs
      //       style={tabStyle}
      //       indicatorColor="secondary"
      //       textColor="secondary"
      //     >
      //       <Avatar style={avatarStyle}>
      //         <PersonOutlineOutlinedIcon />
      //       </Avatar>
      //       <Tab label="home" onClick={this.CustomerHome} />
      //       <Tab label="search bus" onClick={this.SearchBus} />
      //       <Tab label="search car" onClick={this.SearchCar} />
      //       <Tab label="my trips" onClick={this.MyTrips} />
      //       <Tab label="profile" onClick={this.Profile} />
      //       <Tab label="about us" onClick={this.AboutUs} />
      //       <Tab label="contact us" onClick={this.ContactUs} />
      //     </Tabs>
      //   </Paper>
        <Grid style={bgStyle}>
          <Grid style={paperStyle}>
            <Paper elevation={10} style={tabStyle} align="center">
              <Avatar style={avatarStyle}>
                <LockOutlinedIcon />
              </Avatar>
              <h2>About Us</h2>
              <Typography variant="h3"></Typography>
              <Typography style={{ margin: "20 px 0" }}>
                <Box p={3} textAlign="center">
                  BlueBusAndCars is most effficient online bus ticketing &
                  Rental Car Booking platform that has transformed bus travel &
                  Rental Car in the country by bringing ease and convenience to
                  millions of Indians who travel using buses & Cars Founded in
                  2021, BlueBusAndCars is part of emerging Startup online travel
                  company JRDA Limited (NASDAQ: MMYT). By providing widest
                  choice, superior customer service, lowest prices and unmatched
                  benefits, BlueBusAndCars has served over thousand customers.
                  BlueBusAndCars will be a global presence in Future with
                  operations across Indonesia, Singapore, Malaysia, Colombia and
                  Peru apart from India.
                </Box>

                <Typography style={{ margin: "20 px 0" }}>
                  <Box>
                    Ph: ++918407920626 For any Support or Complaints
                    Call Centre Time : 24*7 For Press enquiries, please send
                    email to JRDA@bluebus.com
                  </Box>
                </Typography>
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      // </Grid>
    );
  }
}

export default AboutUs;
