import React from 'react'
import UserServices from '../Service/UserService'
import {
  Avatar,
  Button,
  Grid,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Link from '@material-ui/core/Link'
import Image from '../img/bg.jpg'

class Signup extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dob: '',
      email: '',
      password: '',
      firstName: '',
      gender: '',
      lastName: '',
      phone: '',
    }
    this.signup = this.signup.bind(this)
  }

  signup = (e) => {
    e.preventDefault()
    let user = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: this.state.password,
      gender: this.state.gender,
      dob: this.state.dob,
      phone: this.state.phone,
    }
    UserServices.addUsers(user).then((res) => {
      this.props.history.push('/signin')
    })
  }

  handleChangefirstName = (e) => this.setState({ firstName: e.target.value })
  handleChangelastName = (e) => this.setState({ lastName: e.target.value })
  handleChangeemail = (e) => this.setState({ email: e.target.value })
  handleChangepassword = (e) => this.setState({ password: e.target.value })
  handleChangegender = (e) => this.setState({ gender: e.target.value })
  handleChangedob = (e) => this.setState({ dob: e.target.value })
  handleChangephone = (e) => this.setState({ phone: e.target.value })

  render() {
    const tabStyle = {
      padding: 30,
      height: '80vh',
      width: 280,
      margin: '0px 1050px',
    }

    const buttonStyle = {
      margin: '20px auto',
    }

    const avatarStyle = {
      backgroundColor: '#4db2b5',
    }

    const bgStyle = {
      backgroundImage: `url(${Image})`,
      minHeight: '100vh',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
    }

    const paperStyle = {
      margin: '0px auto',
    }

    return (
      <Grid style={bgStyle}>
        <Grid style={paperStyle}>
          <Paper elevation={10} style={tabStyle} align="center">
            <Avatar style={avatarStyle}>
              <LockOutlinedIcon />
            </Avatar>
            <h2>Sign Up</h2>
            <form>
              <TextField
                label="First Name"
                onChange={this.handleChangefirstName}
                fullWidth
              />
              <TextField
                label="Last Name"
                onChange={this.handleChangelastName}
                fullWidth
              />
              <TextField
                label="Email"
                onChange={this.handleChangeemail}
                fullWidth
              />
              <TextField
                label="Password"
                onChange={this.handleChangepassword}
                type="password"
                fullWidth
              />
              <TextField
                label="Gender"
                onChange={this.handleChangegender}
                fullWidth
              />

              <TextField
                type="date"
                onChange={this.handleChangedob}
                defaultValue="2000-01-01"
                fullWidth
              />
              <TextField
                label="Phone No"
                onChange={this.handleChangephone}
                fullWidth
              />
            </form>
            <Button
              onClick={this.signup}
              style={buttonStyle}
              variant="contained"
              color="primary"
              fullWidth>
              Sign Up
            </Button>
            <Typography>
              Existing Account ?<Link href="/Signin"> SignIn Here</Link>
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

export default Signup
