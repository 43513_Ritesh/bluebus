import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css'
import Signup from './Component/SignUp'
import Index from './Component/Index'
import OwnerSignIn from './Component/Owner/OwnerSignIn'
import SignIn from './Component/SignIn'
import AdminNavbar from './Component/Navbar/AdminNavbar'
import OwnerNavbar from './Component/Navbar/OwnerNavbar'
import CustomerNavbar from './Component/Navbar/CustomerNavbar'
import BusSearch from './Component/Customer/SearchBus'
import CarSearch from './Component/Customer/SearchCar'
import UpdateCompany from './Component/Admin/UpdateCompany'
import CarBooking from './Component/CarBookings'
import BusBooking from './Component/BusBooking'
import CheckOut from './Component/CheckOut'
import Transaction from './Component/Transaction'

function App() {
  return (
    <div className="App">
      {/* <Routes/> */}
      <Router>
        <Switch>
          <Route path="/signin" component={SignIn} />
          <Route path="/signup" component={Signup} />
          <Route path="/ownersignin" component={OwnerSignIn} />
          <Route path="/adminnavbar" component={AdminNavbar} />
          <Route path="/ownernavbar" component={OwnerNavbar} />
          <Route path="/" component={CustomerNavbar} />
          <Route path="/bussearch" component={BusSearch} />
          <Route path="/carsearch" component={CarSearch} />
          <Route path="/updatecompany" component={UpdateCompany} />
          <Route path="/carbooking" component={CarBooking} />
          <Route path="/busbooking" component={BusBooking} />
          <Route path="/checkout" component={CheckOut} />
          <Route path="/transaction" component={Transaction} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
