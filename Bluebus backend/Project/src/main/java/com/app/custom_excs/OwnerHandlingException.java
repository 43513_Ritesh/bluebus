package com.app.custom_excs;

@SuppressWarnings("serial")
public class OwnerHandlingException extends RuntimeException {
	public OwnerHandlingException(String errmsg) {
		super(errmsg);
		System.out.println("in ctor of " + getClass().getName());
	}

}
