package com.app.custom_excs;

@SuppressWarnings("serial")
public class CustomerHandlingException extends RuntimeException {
	public CustomerHandlingException(String errmsg) {
		super(errmsg);
		System.out.println("in ctor of "+getClass().getName());
	}

}
