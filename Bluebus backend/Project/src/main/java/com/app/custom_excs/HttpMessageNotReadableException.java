package com.app.custom_excs;

@SuppressWarnings("serial")
public class HttpMessageNotReadableException extends RuntimeException {
	public HttpMessageNotReadableException(String errmsg) {
		super(errmsg);
		System.out.println("in ctor of " + getClass().getName());
	}

}
