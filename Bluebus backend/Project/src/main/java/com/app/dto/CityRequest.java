package com.app.dto;

public class CityRequest {

	private String city;

	public CityRequest() {

		System.out.println("in ctor of " + getClass().getName());
	}

	public CityRequest(String city) {
		super();
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "CityRequest [city=" + city + "]";
	}

	

}
