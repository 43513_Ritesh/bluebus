package com.app.dto;

public class DateRequest {

	private String bookingDate;

	public DateRequest() {

		System.out.println("in ctor of " + getClass().getName());
	}

	public DateRequest(String bookingDate) {
		super();
		this.bookingDate = bookingDate;
	}

	public String getbookingDate() {
		return bookingDate;
	}

	public void setbookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	@Override
	public String toString() {
		return "bookingDateRequest [bookingDate=" + bookingDate + "]";
	}

}
