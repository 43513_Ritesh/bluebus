package com.app.dto;

public class SourceAndDestRequest {

	private String source;
	private String destination;

	public SourceAndDestRequest() {

		System.out.println("in ctor of " + getClass().getName());
	}

	public SourceAndDestRequest(String source, String destination) {
		super();
		this.source = source;
		this.destination = destination;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	@Override
	public String toString() {
		return "SourceAndDestRequest [source=" + source + ", destination=" + destination + "]";
	}

	

	

}
