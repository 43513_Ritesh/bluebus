package com.app.dto;

import java.time.LocalDate;
import java.time.Period;

public class CarBookingRequest {
	private String startDate;
	private String endDate;
	private int days;
	private double kms;

	public CarBookingRequest() {
		System.out.println("in ctor of " + getClass().getName());
	}

	public CarBookingRequest(String startDate, String endDate, double kms) {
		super();
		System.out.println("in ctor of " + getClass().getName());
		this.startDate = startDate;
		this.endDate = endDate;
//		this.days = days;
		this.kms = kms;
//		this.days = Period.between(LocalDate.parse(startDate), LocalDate.parse(endDate)).getDays();
//		System.out.println();
	}

	public int getDays() {
		return Period.between(LocalDate.parse(startDate), LocalDate.parse(endDate)).getDays();
	}

	public void setDays(int days) {
		this.days = days;
	}

	public double getKms() {
		return kms;
	}

	public void setKms(double kms) {
		this.kms = kms;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return " CarBookingRequest [startDate=" + startDate + ", endDate=" + endDate + ", days=" + days + ", kms=" + kms
				+ " ]";
	}

}
