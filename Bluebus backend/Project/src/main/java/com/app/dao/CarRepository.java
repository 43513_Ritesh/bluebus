package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Car;
import com.app.pojos.Owner;

public interface CarRepository extends JpaRepository<Car, Integer> {
	List<Car> findByOwner(Owner o);

	List<Car> findByCity(String city);

	@Query("select c from Car c join fetch c.owner where c.owner.companyId=:vId and c.carId=:cId")
	Car getCarByOwnerIdCarId(@Param("vId") int companyid,@Param("cId") int carid);

}
