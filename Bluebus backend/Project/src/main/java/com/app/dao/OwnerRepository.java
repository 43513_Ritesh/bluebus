package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Owner;

public interface OwnerRepository extends JpaRepository<Owner, Integer> {
	Optional<Owner> findByEmailAndPassword(String email, String password);

}
