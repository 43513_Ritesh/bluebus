package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Car;
import com.app.pojos.CarBooking;
import com.app.pojos.User;

public interface CarBookingRepository extends JpaRepository<CarBooking, Integer> {
	List<Car> findByUser(User u);

	List<CarBooking> findBookingsByUser(User u);
	
	CarBooking findByCar(Car c);

}
