package com.app.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Bus;
import com.app.pojos.BusBooking;
import com.app.pojos.User;

public interface BusBookingRepository extends JpaRepository<BusBooking, Integer> {
	List<Bus> findByUser(User u);

	@Query("SELECT SUM(b.seats) FROM BusBooking b WHERE b.bus.busId=:busId and b.bookingDate=:bookDate")
	Float getSeatCount(@Param("busId")Integer  bId, @Param("bookDate")LocalDate bDate);
	

	@Query("SELECT SUM(b.seats) FROM BusBooking b")
	Float getSeatCount();
	
	List<BusBooking> findBookingsByUser(User u);

}
