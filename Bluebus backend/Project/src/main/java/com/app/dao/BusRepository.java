package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Bus;
import com.app.pojos.Owner;

public interface BusRepository extends JpaRepository<Bus, Integer> {
	List<Bus> findByOwner(Owner o);

	@Query("select b from Bus b join fetch b.owner where b.owner.companyId=:id")
	List<Bus> getBusListByOwnerId(@Param("id") int companyid);

	List<Bus> findListBusBySourceAndDestination(String source, String destination);

	@Query("select b from Bus b join fetch b.owner where b.owner.companyId=:vId and b.busId=:bId")
	Bus getBusByOwnerIdBusId(@Param("vId") int companyid,@Param("bId") int busid);

}
