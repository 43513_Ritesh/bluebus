package com.app.service;

import java.time.LocalDate;
import java.util.List;

import com.app.pojos.Bus;
import com.app.pojos.BusBooking;
import com.app.pojos.Car;
import com.app.pojos.CarBooking;
import com.app.pojos.User;

public interface IUserService {
	// add a method for customer login
	User authenticateCustomer(String em, String pwd);

	User signUpCustomer(User user);

	String deleteCustomer(int id);

	User getCustomerById(int id);

	User getVendorDetails(int vid);

	String updateVendorDetails(User vendor);

	List<Bus> getAllBuses();

	List<Car> getAllCars();

	List<Car> findCarByCity(String city);

	BusBooking addBooking(Integer bId, Integer uId, BusBooking b);

	Float checkSeatCount(Integer bId, LocalDate bDate);

	List<Bus> getBusBySourceAndDest(String source, String destination);

	List<BusBooking> getUserBookings(Integer uId);

	CarBooking addCarBooking(Integer cId, Integer uId, CarBooking c);

	Car checkCarAvailable(Integer cId);

	BusBooking cancelBusBooking(Integer bookId);
}
