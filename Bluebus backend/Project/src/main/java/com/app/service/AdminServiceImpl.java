package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.BusRepository;
import com.app.dao.CarRepository;
import com.app.dao.OwnerRepository;
import com.app.pojos.Bus;
import com.app.pojos.Car;
import com.app.pojos.Owner;

@Service
@Transactional
public class AdminServiceImpl implements IAdminService {
	@Autowired
	OwnerRepository ownerRepo;
	@Autowired
	BusRepository busRepo;
	@Autowired
	CarRepository carRepo;

	@Override
	public Owner signUpOwner(Owner Owner) {
		return ownerRepo.save(Owner);

	}

	@Override
	public String deleteOwner(int id) {
		ownerRepo.deleteById(id);
		return "Owner deleted by id :" + id;

	}

	@Override
	public List<Owner> getAllOwners() {
		return ownerRepo.findAll();
	}

	@Override
	public Owner getOwnerById(int vid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateOwnerDetails(Owner owner) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Bus> getAllBuses() {
//		system generated method to get all instances in database
		return busRepo.findAll();
	}

	@Override
	public List<Car> getAllCars() {
		return carRepo.findAll();
	}

}
