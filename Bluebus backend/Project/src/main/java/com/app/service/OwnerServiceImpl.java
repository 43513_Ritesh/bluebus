package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.custom_excs.CustomerHandlingException;
import com.app.dao.BusRepository;
import com.app.dao.CarRepository;
import com.app.dao.OwnerRepository;
import com.app.pojos.Bus;
import com.app.pojos.Car;
import com.app.pojos.Owner;

@Service
@Transactional
public class OwnerServiceImpl implements IOwnerService {
	@Autowired
	OwnerRepository ownerRepo;
	@Autowired
	BusRepository busRepo;
	@Autowired
	CarRepository carRepo;

	@Override
	public Owner authenticateOwner(String em, String pwd) {
		return ownerRepo.findByEmailAndPassword(em, pwd)
				.orElseThrow(() -> new CustomerHandlingException("Invalid Credentials!!!!"));
	}

//here adding bus via owners company_id getting from owners page, so unique valid id will be sent 
//	so no need to handle exception of wrong id sent from front-end
	@Override
	public String addBus(Bus bus, Integer id) {
		// to get owner by given id
		Owner o = ownerRepo.findById(id).get();// return persistent pojo
		// "o.addBus(bus)" adds buses and checks if added or not..written in pojo
		if (o.addBus(bus))
			return "Bus added successfully";
		return "Bus adding failed";
	}

	@Override
	public String addCar(Car car, Integer id) {
		// to get owner by given id
		Owner o = ownerRepo.findById(id).get();
		// "o.addCar(car)" adds cars and checks if added or not..written in pojo
		if (o.addCar(car))
			return "Car added successfully";
		return "Car adding failed";
	}

	@Override
	public List<Bus> getBusesById(int id) {
//		Owner o=ownerRepo.findById(id).get();
//		List<Bus> busList=busRepo.findByOwner(o);
		List<Bus> busList = busRepo.getBusListByOwnerId(id);
		return busList;
	}

	@Override
	public List<Car> getCarsById(int id) {
		Owner o = ownerRepo.findById(id).get();
		List<Car> carList = carRepo.findByOwner(o);
		return carList;
	}

	@Override
	public Bus getSelectedBusById(Integer vid, Integer bid) {
		return busRepo.getBusByOwnerIdBusId(vid.intValue(), bid.intValue());
//		return null;
	}

	@Override
	public Bus updateSelectedBus(Integer oid, Bus bus) {
		Owner o = ownerRepo.findById(oid).get();
		bus.setOwner(o);
		if (busRepo.findById(bus.getBusId()).get().getBookings().size() == 0 && o != null)
			return busRepo.save(bus);
		else
			return null;
	}

	@Override
	public Car getSelectedCarById(Integer oid, Integer cid) {
		return carRepo.getCarByOwnerIdCarId(oid.intValue(), cid.intValue());
	}

	@Override
	public Car updateSelectedCar(Integer oid, Car car) {
		System.out.println("in update Selected Car");
		Owner o = ownerRepo.findById(oid).get();
		car.setOwner(o);
		if (carRepo.findById(car.getCarId()).get().getBookings().size() == 0 && o != null)
			return carRepo.save(car);
		else
			return null;
	}

	@Override
	public String deleteCarByCarId(Integer id) {
		if (carRepo.findById(id).get().getBookings().size() == 0) {
			carRepo.deleteById(id);
			return "Car deleted with id" + id;
		} else
			return "Car has bookings";

	}

	@Override
	public String deleteBusByBusId(Integer id) {
		if (busRepo.findById(id).get().getBookings().size() == 0) {
			busRepo.deleteById(id);
			return "Bus deleted with id" + id;
		} else
			return "Bus has bookings";
	}

}
