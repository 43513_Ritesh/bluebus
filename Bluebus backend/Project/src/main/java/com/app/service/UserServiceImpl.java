package com.app.service;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.custom_excs.CustomerHandlingException;
import com.app.custom_excs.failedToGetUserByIdException;
import com.app.dao.BusBookingRepository;
import com.app.dao.BusRepository;
import com.app.dao.CarBookingRepository;
import com.app.dao.CarRepository;
import com.app.dao.UserRepository;
import com.app.pojos.Bus;
import com.app.pojos.BusBooking;
import com.app.pojos.Car;
import com.app.pojos.CarBooking;
import com.app.pojos.Status;
import com.app.pojos.User;

@Service
@Transactional
public class UserServiceImpl implements IUserService {
	@Autowired
	UserRepository userRepo;
	@Autowired
	BusRepository busRepo;
	@Autowired
	CarRepository carRepo;
	@Autowired
	BusBookingRepository busBookingRepo;
	@Autowired
	CarBookingRepository carBookingRepo;

	@Override
	public User authenticateCustomer(String em, String pwd) {
		// TODO Auto-generated method stub
		return userRepo.authenticateUser(em, pwd)
				.orElseThrow(() -> new CustomerHandlingException("Invalid Credentials!!!!"));

	}

	@Override
	public User signUpCustomer(User user) {
		return userRepo.save(user);

	}

	@Override
	public String deleteCustomer(int id) {
		userRepo.deleteById(id);
		return "user deleted by id :" + id;

	}

	@Override
	public User getCustomerById(int id) {
		return userRepo.findById(id)
				.orElseThrow(() -> new failedToGetUserByIdException("Such user with id does not exist!!!!"));
	}

	@Override
	public User getVendorDetails(int vid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateVendorDetails(User vendor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Bus> getAllBuses() {
//		system generated method to get all instances in database
		return busRepo.findAll();
	}

	@Override
	public List<Car> getAllCars() {
		return carRepo.findAll();
	}

	@Override
	public List<Car> findCarByCity(String city) {
		System.out.println(city);
		return carRepo.findByCity(city);
	}

	@Override
	public BusBooking addBooking(Integer bId, Integer uId, BusBooking b) {
		System.out.println(b.toString());
		System.out.println("in addbooking of user service layer");
		Bus bus = busRepo.getOne(bId);
		b.setPayment(bus.getFarePerSeat() * b.getSeats());
		b.setBus(bus);
		b.setUser(userRepo.getOne(uId));
		System.out.println(b.toString());
		return busBookingRepo.save(b);
//		return null;
	}

	@Override
	public Float checkSeatCount(Integer bId, LocalDate bDate) {
		Float b = busBookingRepo.getSeatCount(bId, bDate);
//		Float b=busBookingRepo.getSeatCount();
		System.out.println(b);
		return b;
//		return null;
	}

	@Override
	public List<Bus> getBusBySourceAndDest(String source, String destination) {
		return busRepo.findListBusBySourceAndDestination(source, destination);
	}

	@Override
	public List<BusBooking> getUserBookings(Integer uId) {
		return busBookingRepo.findBookingsByUser((userRepo.getOne(uId)));
//			busBookingRepo.
//		return null;
	}

	@Override
	public CarBooking addCarBooking(Integer cId, Integer uId, CarBooking c) {
		System.out.println("in addbooking of user service layer");
		Car car = carRepo.getOne(cId);
		if ((c.getKms() - 200) > 0) {
			c.setPayment(car.getPricePerDay() * c.getDays() + car.getRsPerKm() * (c.getKms() - 200));
		} else {
			c.setPayment(car.getPricePerDay() * c.getDays());
		}
		c.setCar(car);
		c.setUser(userRepo.getOne(uId));
		System.out.println(c.toString());
		return carBookingRepo.save(c);
	}

	@Override
	public Car checkCarAvailable(Integer cId) {
		Car c = carRepo.findById(cId).get();
		if (carBookingRepo.findByCar(c) == null)
			return c;
		return null;
	}

	@Override
	public BusBooking cancelBusBooking(Integer bookId) {
		BusBooking booking = busBookingRepo.findById(bookId).get();
		booking.setStatus(Status.INACTIVE);
		booking.setSeats(0);
		return booking;
	}

}
