package com.app.service;

import java.util.List;

import com.app.pojos.Bus;
import com.app.pojos.Car;
import com.app.pojos.Owner;

public interface IAdminService {
	Owner signUpOwner(Owner Owner);

	String deleteOwner(int id);

	Owner getOwnerById(int vid);

	String updateOwnerDetails(Owner Owner);

	List<Owner> getAllOwners();

	List<Bus> getAllBuses();

	List<Car> getAllCars();

}
