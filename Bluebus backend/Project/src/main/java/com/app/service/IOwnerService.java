package com.app.service;

import java.util.List;

import com.app.pojos.Bus;
import com.app.pojos.Car;
import com.app.pojos.Owner;

public interface IOwnerService {
	// add a method for Owner login
	Owner authenticateOwner(String em, String pwd);

	List<Bus> getBusesById(int id);

	List<Car> getCarsById(int id);

//	String deleteCarByCarId(Integer id);

	String addBus(Bus bus, Integer id);

	String addCar(Car car, Integer id);

	Bus getSelectedBusById(Integer vid, Integer bid);

	Bus updateSelectedBus(Integer oid, Bus bus);

	Car getSelectedCarById(Integer oid, Integer cid);

	Car updateSelectedCar(Integer oid, Car car);

	String deleteCarByCarId(Integer id);

	String deleteBusByBusId(Integer id);

}
