package com.app.pojos;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user", uniqueConstraints = { @UniqueConstraint(columnNames = { "email" }) })
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;

	@Column(length = 55)
	private String firstName;

	@Column(length = 55)
	private String lastName;

	@Column(nullable = false)
	@NotBlank(message = "Email is required")
	@Length(min = 5, max = 25, message = "Invalid Email length")
	@Email(message = "Invalid email format")
	private String email = "abc@gmail.com";

	@Column(length = 20, nullable = false)
	@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Invalid password!")
	private String password;

	@Column(length = 55)
	private String phone;

	@Column(length = 50)
	private String gender;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@PastOrPresent(message = "invalid date of birth")
	private LocalDate dob;

	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private Role role;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("user") // To tell SC : ignore property of list of accts during ser n de-serial .
	@JsonIgnore
	private List<BusBooking> bookings;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("user") // To tell SC : ignore property of list of accts during ser n de-serial .
	@JsonIgnore
	private List<CarBooking> carBookings;

	public User() {
		System.out.println("in ctor of " + getClass().getName());
	}

	public User(int userId, String firstName, String lastName,
			@NotBlank(message = "Email is required") @Length(min = 5, max = 25, message = "Invalid Email length") @Email(message = "Invalid email format") String email,
			@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Invalid password!") String password,
			String phone, String gender, @PastOrPresent(message = "invalid date of birth") LocalDate dob, Role role) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.gender = gender;
		this.dob = dob;
		this.role = role;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
//helper methods
	public Boolean addBusBooking(BusBooking b) {
		bookings.add(b);
		b.setUser(this);
		return bookings.contains(b);
	}
	
	public Boolean addCarBooking(CarBooking c) {
		carBookings.add(c);
		c.setUser(this);
		return carBookings.contains(c);
	}
	
	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", phone=" + phone + ", gender=" + gender + ", dob=" + dob + ", role=" + role + "]";
	}

}