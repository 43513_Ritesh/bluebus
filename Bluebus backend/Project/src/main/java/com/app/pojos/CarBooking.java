package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.FutureOrPresent;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "carBooking")
public class CarBooking {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookingId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@FutureOrPresent(message = "invalid date")
	@Column(length = 57)
	private LocalDate bookingDate;
	@Column(length = 57)
	private double payment;
	@Column(length = 57)
	private int days;
	@Column(length = 57)
	private double kms;
	@Enumerated(EnumType.STRING)
	@Column(length = 57)
	private Status status;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "carId")
	@JsonIgnoreProperties("bookings") // remain
	@JsonIgnore
	private Car car;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	@JsonIgnoreProperties("bookings") // remain
	@JsonIgnore
	private User user;

	public CarBooking() {
		System.out.println("in ctor of param less " + getClass().getName());
	}

	public CarBooking(@FutureOrPresent(message = "invalid date") LocalDate bookingDate, double payment, int days,
			double kms, Status status) {
		super();
		this.bookingDate = bookingDate;
		this.payment = payment;
		this.days = days;
		this.kms = kms;
		this.status = status;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public LocalDate getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDate bookingDate) {
		this.bookingDate = bookingDate;
	}

	public double getPayment() {
		return payment;
	}

	public void setPayment(double payment) {
		this.payment = payment;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public double getKms() {
		return kms;
	}

	public void setKms(double kms) {
		this.kms = kms;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "CarBooking [bookingId=" + bookingId + ", bookingDate=" + bookingDate + ", payment=" + payment
				+ ", days=" + days + ", kms=" + kms + ", status=" + status + "]";
	}

}
