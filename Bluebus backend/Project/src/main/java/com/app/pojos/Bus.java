package com.app.pojos;
//busId	company_id	busName	busNo	Driver_name	source	destination	Time	Fare per Seat

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "bus", uniqueConstraints = { @UniqueConstraint(columnNames = { "busNo" }) })
public class Bus {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int busId;
//	source	destination	Time	
	@Column(length = 55)
	private String busName;
	@Column(length = 55, nullable = false)
	private String busNo;
	@Column(length = 200)
	private String driverName;
	@Column(length = 55)
	private String source;
	@Column(length = 55)
	private String destination;
	@Column(length = 17)
	private LocalTime time;
	@Column(length = 50)
	private Double farePerSeat;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "companyId")
	@JsonIgnoreProperties("buses,cars")
	@JsonIgnore
	private Owner owner;

	@OneToMany(mappedBy = "bus", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("bus") // To tell SC : ignore property of list of accts during ser n de-serial .
	@JsonIgnore
	private List<BusBooking> bookings;

	public Bus() {
		System.out.println("in ctor of " + getClass().getName());
		bookings=new ArrayList<BusBooking>();
	}

	public Bus(String busName, String busNo, String driverName, String source, String destination, LocalTime time,
			Double farePerSeat) {
		super();
		this.busName = busName;
		this.busNo = busNo;
		this.driverName = driverName;
		this.source = source;
		this.destination = destination;
		this.time = time;
		this.farePerSeat = farePerSeat;
	}

	public int getBusId() {
		return busId;
	}

	public void setBusId(int busId) {
		this.busId = busId;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

	public String getBusNo() {
		return busNo;
	}

	public void setBusNo(String busNo) {
		this.busNo = busNo;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public Double getFarePerSeat() {
		return farePerSeat;
	}

	public void setFarePerSeat(Double farePerSeat) {
		this.farePerSeat = farePerSeat;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<BusBooking> getBookings() {
		return bookings;
	}

	public void setBookings(List<BusBooking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public String toString() {
		return "Bus [busId=" + busId + ", busName=" + busName + ", busNo=" + busNo + ", driverName=" + driverName
				+ ", source=" + source + ", destination=" + destination + ", time=" + time + ", farePerSeat="
				+ farePerSeat + "]";
	}

}
