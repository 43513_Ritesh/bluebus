package com.app.pojos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "car", uniqueConstraints = { @UniqueConstraint(columnNames = { "carNo" }) })
public class Car {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int carId;
	@Column(length = 55)
	private String carName;
	@Column(length = 55, nullable = false)
	private String carNo;
	@Column(length = 50)
	private String city;
	@Column(length = 50)
	private Double pricePerDay;
	@Column(length = 50)
	private Double rsPerKm;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "companyId")
	@JsonIgnoreProperties("buses,cars")
	@JsonIgnore
	private Owner owner;

	@OneToMany(mappedBy = "car", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("car") // To tell SC : ignore property of list of accts during ser n de-serial .
	@JsonIgnore
	private List<CarBooking> bookings;

	public Car() {
		System.out.println("in ctor of " + getClass().getName());
	}

	public Car(int carId, String carName, String carNo, String city, Double pricePerDay, Double rsPerKm) {
		super();
		this.carId = carId;
		this.carName = carName;
		this.carNo = carNo;
		this.city = city;
		this.pricePerDay = pricePerDay;
		this.rsPerKm = rsPerKm;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Double getPricePerDay() {
		return pricePerDay;
	}

	public void setPricePerDay(Double pricePerDay) {
		this.pricePerDay = pricePerDay;
	}

	public Double getRsPerKm() {
		return rsPerKm;
	}

	public void setRsPerKm(Double rsPerKm) {
		this.rsPerKm = rsPerKm;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<CarBooking> getBookings() {
		return bookings;
	}

	public void setBookings(List<CarBooking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public String toString() {
		return "Car [carId=" + carId + ", carName=" + carName + ", carNo=" + carNo + ", city=" + city + ", pricePerDay="
				+ pricePerDay + ", rsPerKm=" + rsPerKm + "]";
	}

}
