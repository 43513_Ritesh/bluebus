package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.FutureOrPresent;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "busBooking")
public class BusBooking {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookingId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@FutureOrPresent(message = "invalid date")
	@Column(length = 57)
	private LocalDate bookingDate;
	@Column(length = 57)
	private double payment;
	@Column(length = 57)
	private int seats;
	@Enumerated(EnumType.STRING)
	@Column(length = 57)
	private Status status;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "busId")
	@JsonIgnoreProperties("bookings") // remain
	@JsonIgnore
	private Bus bus;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	@JsonIgnoreProperties("bookings") // remain
	@JsonIgnore
	private User user;

	public BusBooking() {
		System.out.println("in ctor of param less " + getClass().getName());
	}

	public BusBooking(LocalDate bookingDate, double payment, int seats, Status status) {
		super();
		this.bookingDate = bookingDate;
		this.payment = payment;
		this.seats = seats;
		this.status = status;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public LocalDate getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDate bookingDate) {
		this.bookingDate = bookingDate;
	}

	public double getPayment() {
		return payment;
	}

	public void setPayment(double payment) {
		this.payment = payment;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Bus getBus() {
		return bus;
	}

	public void setBus(Bus bus) {
		this.bus = bus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "BusBooking [bookingId=" + bookingId + ", bookingDate=" + bookingDate + ", payment=" + payment + ", seats="
				+ seats + ", status=" + status + "]";
	}

}
