package com.app.pojos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "owner", uniqueConstraints = { @UniqueConstraint(columnNames = { "email" }) })
public class Owner {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int companyId;

	@Column(length = 55)
	private String companyName;

	@Column(length = 55)
	private String owner;

	@Column(nullable = false)
	@NotBlank(message = "Email is required")
	@Length(min = 5, max = 55, message = "Invalid Email length")
	@Email(message = "Invalid email format")
	private String email;

	@Column(length = 20, nullable = false)
	@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Invalid password!")
	private String password;

	@Column(length = 55)
	private String phone;

	@Column(length = 55)
	private String registrationNo;

	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean busService;

	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean carService;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("owner") // To tell SC : ignore property of list of accts during ser n de-serial .
	@JsonIgnore
	private List<Bus> buses;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("owner") // To tell SC : ignore property of list of accts during ser n de-serial .
	@JsonIgnore
	private List<Car> cars;

	public Owner() {
		System.out.println("in ctor of paramless " + getClass().getName());
	}

	public Owner(int companyId, String companyName, String owner,
			@NotBlank(message = "Email is required") @Length(min = 5, max = 25, message = "Invalid Email length") @Email(message = "Invalid email format") String email,
			@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Invalid password!") String password,
			String phone, String registrationNo, boolean busService, boolean carService) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.owner = owner;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.registrationNo = registrationNo;
		this.busService = busService;
		this.carService = carService;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public boolean isBusService() {
		return busService;
	}

	public void setBusService(boolean busService) {
		this.busService = busService;
	}

	public boolean isCarService() {
		return carService;
	}

	public void setCarService(boolean carService) {
		this.carService = carService;
	}

	public List<Bus> getBuses() {
		return buses;
	}

	public void setBuses(List<Bus> buses) {
		this.buses = buses;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	// helper methods to add n remove bus
	public Boolean addBus(Bus b) {
		buses.add(b);
		b.setOwner(this);
		return buses.contains(b);
	}

	public void removeBus(Bus b) {
		buses.remove(b);
		b.setOwner(null);
	}

	// helper methods to add n remove car
	public Boolean addCar(Car c) {
		cars.add(c);
		c.setOwner(this);
		return cars.contains(c);
	}

	public void removeCar(Car b) {
		cars.remove(b);
		b.setOwner(null);
	}

}
