package com.app.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.dto.CarBookingRequest;
import com.app.dto.CityRequest;
import com.app.dto.DateRequest;
import com.app.dto.LoginRequest;
import com.app.dto.SourceAndDestRequest;
import com.app.pojos.BusBooking;
import com.app.pojos.CarBooking;
import com.app.pojos.Role;
import com.app.pojos.Status;
import com.app.pojos.User;
import com.app.service.IUserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/")
public class UserController {
	// dependency : service layer i/f
	@Autowired
	private IUserService userService;

	public UserController() {
		// TODO Auto-generated constructor stub
		System.out.println("in ctor of " + getClass().getName());
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateCustomer(@RequestBody LoginRequest request) {
		System.out.println("in auth " + request);
		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
		return new ResponseEntity<>(userService.authenticateCustomer(request.getEmail(), request.getPassword()),
				HttpStatus.OK);
	}

	@PostMapping("/signup")
	public ResponseEntity<?> signUpCustomer(@RequestBody User user) {
		System.out.println("in auth " + user);
		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
		user.setRole(Role.CUSTOMER);
		return new ResponseEntity<>(userService.signUpCustomer(user), HttpStatus.OK);
	}

	@GetMapping("/getbyid/{id}")
	public ResponseEntity<?> getCustomerById(@PathVariable Integer id) {
		System.out.println("in auth " + id);
		return new ResponseEntity<>(userService.getCustomerById(id), HttpStatus.OK);
	}

	@DeleteMapping("/delbyid/{id}")
	public ResponseEntity<?> delCustomerById(@PathVariable Integer id) {
		System.out.println("in auth " + id);
		return new ResponseEntity<>(userService.deleteCustomer(id), HttpStatus.OK);
	}

	// add req handling method to show update form
	@GetMapping("/update/{vid}")
	public ResponseEntity<?> getCustomerForUpdateById(@PathVariable Integer vid) {
		System.out.println("in auth " + vid);
		return new ResponseEntity<>(userService.getCustomerById(vid), HttpStatus.OK);
	}

	// add req handling method for processing update form NOT YET COMPLETED
	@PostMapping("/update")
	public ResponseEntity<?> processUpdateForm(User vendor, RedirectAttributes flashMap) {
		System.out.println("process update form " + vendor);// vendor : DETACHED POJO containing updated state
		return (ResponseEntity<?>) flashMap.addFlashAttribute("message", userService.updateVendorDetails(vendor));

	}

	@GetMapping("/show/allbuses")
	public ResponseEntity<?> getAllBuses() {
		System.out.println("get all buses" + getClass().getName());
		return new ResponseEntity<>(userService.getAllBuses(), HttpStatus.OK);
	}

	@GetMapping("/show/allcars")
	public ResponseEntity<?> getAllCars() {
		System.out.println("get all cars" + getClass().getName());
		return new ResponseEntity<>(userService.getAllCars(), HttpStatus.OK);
	}

	@PostMapping("/show/carsbycity")
	public ResponseEntity<?> getCarsByCity(@RequestBody CityRequest city) {
		System.out.println("get cars by city" + city);
		return new ResponseEntity<>(userService.findCarByCity(city.getCity()), HttpStatus.OK);
	}

	@PostMapping("/addbooking/{bId}/{uId}")
	public ResponseEntity<?> addBooking(@PathVariable Integer bId, @PathVariable Integer uId,
			@RequestBody BusBooking b) {
		b.setStatus(Status.ACTIVE);
		System.out.println(b.toString());
		System.out.println("add booking by busId " + bId + " and to userId " + uId);
		return new ResponseEntity<>(userService.addBooking(bId, uId, b), HttpStatus.OK);
	}

	@PostMapping("/addcarbooking/{cId}/{uId}")
	public ResponseEntity<?> addCarBooking(@PathVariable Integer cId, @PathVariable Integer uId,
			@RequestBody CarBookingRequest cBR) {
		CarBooking c = new CarBooking(LocalDate.parse(cBR.getStartDate()), 0.0, cBR.getDays() + 1, cBR.getKms(),
				Status.ACTIVE);
//		System.out.println(cBR.toString());
//		System.out.println("add booking by busId " + cId + " and to userId " + uId);
		return new ResponseEntity<>(userService.addCarBooking(cId, uId, c), HttpStatus.OK);
	}

	@GetMapping("/seatcount/{bId}")
	public ResponseEntity<?> checkSeatCount(@PathVariable Integer bId, @RequestBody DateRequest bookDate) {
		System.out.println("check Seat Count for bus " + bId + " with date " + bookDate.getbookingDate());
		return new ResponseEntity<>(userService.checkSeatCount(bId, LocalDate.parse(bookDate.getbookingDate())),
				HttpStatus.OK);
	}

	@PostMapping("/getavailablebuses")
	public ResponseEntity<?> getBusBySourceAndDest(@RequestBody SourceAndDestRequest request) {
		System.out.println("get Bus By Source And Dest " + request);
		return new ResponseEntity<>(userService.getBusBySourceAndDest(request.getSource(), request.getDestination()),
				HttpStatus.OK);
	}

	@GetMapping("/getbookings/{uId}")
	public ResponseEntity<?> getUserBookings(@PathVariable Integer uId) {
		System.out.println("get booking by user id " + uId);
		return new ResponseEntity<>(userService.getUserBookings(uId), HttpStatus.OK);
	}

	@GetMapping("/caravailability/{cId}")
	public ResponseEntity<?> checkCarAvailableOrNot(@PathVariable Integer cId) {
		System.out.println("check Car " + cId + " Available Or Not");
		return new ResponseEntity<>(userService.checkCarAvailable(cId), HttpStatus.OK);
	}

	@PostMapping("/cancel/bus/booking/{bookId}")
	public ResponseEntity<?> cancleBusBooking(@PathVariable Integer bookId) {
		System.out.println("cancel Bus Booking by Id " + bookId);
		return new ResponseEntity<>(userService.cancelBusBooking(bookId), HttpStatus.OK);
	}

	// 1.source dest (date optional)
}