package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.LoginRequest;
import com.app.pojos.Bus;
import com.app.pojos.Car;
import com.app.service.IOwnerService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/owner")
public class OwnerController {
	@Autowired
	private IOwnerService ownerService;

	public OwnerController() {
		// TODO Auto-generated constructor stub
		System.out.println("in ctor of " + getClass().getName());
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateOwner(@RequestBody LoginRequest request) {
		System.out.println("in auth " + request);
		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
		return new ResponseEntity<>(ownerService.authenticateOwner(request.getEmail(), request.getPassword()),
				HttpStatus.OK);
	}

	@PostMapping("/addbus/byownerid/{id}")
	public ResponseEntity<?> addBus(@RequestBody Bus bus, @PathVariable Integer id) {
		System.out.println("in adding bus " + bus);
		return new ResponseEntity<>(ownerService.addBus(bus, id), HttpStatus.OK);
	}

	@PostMapping("/addcar/byownerid/{id}")
	public ResponseEntity<?> addCar(@RequestBody Car car, @PathVariable Integer id) {
		System.out.println("in adding car " + car);
		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
//		owner.setService(Service.CAR);
		return new ResponseEntity<>(ownerService.addCar(car, id), HttpStatus.OK);
	}

	@GetMapping("/getbuses/byownerid/{id}")
	public ResponseEntity<?> getOwnerBusesById(@PathVariable Integer id) {
		System.out.println("in get buses by owner id  " + id);
		return new ResponseEntity<>(ownerService.getBusesById(id), HttpStatus.OK);
	}

	@GetMapping("/getcars/byownerid/{id}")
	public ResponseEntity<?> getOwnerCarsById(@PathVariable Integer id) {
		System.out.println("in get buses by owner id  " + id);
		return new ResponseEntity<>(ownerService.getCarsById(id), HttpStatus.OK);
	}

	@GetMapping("/getselectedbus/byownerid/bybusid/{vid}/{bid}")
	public ResponseEntity<?> getOwnerBusesById(@PathVariable Integer vid, @PathVariable Integer bid) {
		System.out.println("in get selected bus by owner id  " + vid + " and by busId " + bid);
		return new ResponseEntity<>(ownerService.getSelectedBusById(vid, bid), HttpStatus.OK);
	}

	@PostMapping("/updatebus/byownerid/{oid}")
	public ResponseEntity<?> updateBus(@PathVariable Integer oid, @RequestBody Bus bus) {
		System.out.println("update bus " + bus);
		return new ResponseEntity<>(ownerService.updateSelectedBus(oid, bus), HttpStatus.OK);
	}

	@GetMapping("/getselectedcar/byownerid/bycarid/{oid}/{cid}")
	public ResponseEntity<?> getOwnerCarById(@PathVariable Integer oid, @PathVariable Integer cid) {
		System.out.println("in get selected car by owner id  " + oid + " and by carId " + cid);
		return new ResponseEntity<>(ownerService.getSelectedCarById(oid, cid), HttpStatus.OK);
	}

	@PostMapping("/updatecar/byownerid/{oid}")
	public ResponseEntity<?> updateCar(@PathVariable Integer oid, @RequestBody Car car) {
		System.out.println("update car " + car);
		return new ResponseEntity<>(ownerService.updateSelectedCar(oid, car), HttpStatus.OK);
	}

	@DeleteMapping("/delcar/bycarid/{id}")
	public ResponseEntity<?> delCarByCarId(@PathVariable Integer id) {
		System.out.println("in del car by id " + id);
		return new ResponseEntity<>(ownerService.deleteCarByCarId(id), HttpStatus.OK);
	}

	@DeleteMapping("/delbus/bybusid/{id}")
	public ResponseEntity<?> delBusByBusId(@PathVariable Integer id) {
		System.out.println("in del bus by id " + id);
		return new ResponseEntity<>(ownerService.deleteBusByBusId(id), HttpStatus.OK);
	}
}
