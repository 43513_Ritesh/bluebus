package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Owner;
import com.app.service.IAdminService;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private IAdminService adminService;

	public AdminController() {
		// TODO Auto-generated constructor stub
		System.out.println("in ctor of " + getClass().getName());
	}

	@PostMapping("/addowner")
	public ResponseEntity<?> signUpOwner(@RequestBody Owner owner) {
		System.out.println("in auth " + owner);
		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
//		owner.setService(Service.CAR);
		return new ResponseEntity<>(adminService.signUpOwner(owner), HttpStatus.OK);
	}

	@GetMapping("/getallowner")
	public ResponseEntity<?> getAllOwners() {
		System.out.println("in get all owners ");
		return new ResponseEntity<>(adminService.getAllOwners(), HttpStatus.OK);
	}

	@GetMapping("/getownerbyid/{id}")
	public ResponseEntity<?> getOwnerById(@PathVariable Integer id) {
		System.out.println("in auth " + id);
		return new ResponseEntity<>(adminService.getOwnerById(id), HttpStatus.OK);
	}

	@DeleteMapping("/delowner/byid/{id}")
	public ResponseEntity<?> delOwnerById(@PathVariable Integer id) {
		System.out.println("in auth " + id);
		return new ResponseEntity<>(adminService.deleteOwner(id), HttpStatus.OK);
	}

}
