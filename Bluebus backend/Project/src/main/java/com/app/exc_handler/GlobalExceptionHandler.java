package com.app.exc_handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.app.custom_excs.CustomerHandlingException;
import com.app.custom_excs.OwnerHandlingException;
import com.app.custom_excs.failedToGetOwnerByIdException;
import com.app.custom_excs.failedToGetUserByIdException;
import com.app.dto.ErrorResponse;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(CustomerHandlingException.class)
	public ResponseEntity<ErrorResponse> handleCustomerHandlingException(CustomerHandlingException e) {
		System.out.println("in handle customer exc");
		return new ResponseEntity<>(new ErrorResponse("Invalid Login", e.getMessage()), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(failedToGetUserByIdException.class)
	public ResponseEntity<ErrorResponse> handleFailedToGetUserByIdException(failedToGetUserByIdException e) {
		System.out.println("in handle customer exc");
		return new ResponseEntity<>(new ErrorResponse("Failed to get User", e.getMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(OwnerHandlingException.class)
	public ResponseEntity<ErrorResponse> handleOwnerHandlingException(OwnerHandlingException e) {
		System.out.println("in handle owner exc");
		return new ResponseEntity<>(new ErrorResponse("Invalid Login", e.getMessage()), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(failedToGetOwnerByIdException.class)
	public ResponseEntity<ErrorResponse> handleFailedToGetOwnerByIdException(failedToGetOwnerByIdException e) {
		System.out.println("in handle owner exc");
		return new ResponseEntity<>(new ErrorResponse("Failed to get Owner", e.getMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> handleException(org.hibernate.exception.ConstraintViolationException e) {
		System.out.println("in handle customer exc");
		return new ResponseEntity<>(new ErrorResponse("Entered data already exists or wrong", e.getMessage()), HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(javax.validation.ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> handleException(javax.validation.ConstraintViolationException e) {
		System.out.println("in handle customer exc");
		return new ResponseEntity<>(new ErrorResponse("Validation Failed", e.getMessage()), HttpStatus.NOT_ACCEPTABLE);
	}

//	@ExceptionHandler(HttpMessageNotReadableException.class)
//	public ResponseEntity<ErrorResponse> handleException(HttpMessageNotReadableException e) {
//		System.out.println("in handle customer exc");
//		return new ResponseEntity<>(new ErrorResponse(" Not one of the values accepted for Enum class,choose correct option", e.getMessage()),
//				HttpStatus.NOT_ACCEPTABLE);
//	}
//	@ExceptionHandler(Exception.class)
//	public ResponseEntity<ErrorResponse> handleException(Exception e) {
//		System.out.println("in handle customer exc");
//		return new ResponseEntity<>(new ErrorResponse(" Exception", e.getMessage()), HttpStatus.NOT_ACCEPTABLE);
//	}

}
